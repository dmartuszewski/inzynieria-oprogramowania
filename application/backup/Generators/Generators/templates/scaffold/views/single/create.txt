@section('main')

<h1>Dodaj {{Model}}</h1>

{{ Form::open(['route' => '{{models}}.store']) }}
	<ul>
{{formElements}}
		<li>
			{{ Form::submit('Wyślij', array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


