-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 10, 2014 at 11:11 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `io`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `isbn` varchar(45) NOT NULL,
  `author` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL COMMENT '	',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `description`, `isbn`, `author`, `year`, `created_at`, `updated_at`) VALUES
(1, 'Scrum. O zwinnym zarządzaniu projektami. Wydanie II rozszerzone', 'Współczesny świat w dużej mierze przeniósł się do wnętrza komputerów. Nie chodzi tylko o internet, ale także o bazy danych najrozmaitszych firm i urzędów, programy do obsługi maszyn i sprzętów AGD, skomplikowane systemy logistyczne, magazynowe, handlowe i wszelkie inne. Bez odpowiedniego oprogramowania żaden z tych elementów rzeczywistości nie będzie właściwie działał, a to może być przyczyną małych kłopotów lub wielkich zagrożeń. Jednak stworzenie takiego oprogramowania nie jest rzeczą prostą, tym bardziej, że w trakcie pracy nad nim zawsze trzeba liczyć się z modyfikacją pierwotnych założeń. Odpowiedzią na to wyzwanie jest właśnie Scrum.', '978-83-246-2519-2', 'Mariusz Chrapko', 2014, '2014-12-05 12:43:37', '2014-12-05 09:43:43'),
(2, 'Zwinne wytwarzanie oprogramowania. Najlepsze zasady, wzorce i praktyki', 'Czasy kaskadowego tworzenia projektów odchodzą w niepamięć. Obecne tempo rozwoju aplikacji i rynku nie pozwala poświęcać miesięcy na analizę, tworzenie dokumentacji, projektowanie, a na końcu wytwarzanie, testowanie i wdrażanie. Produkt musi być dostępny błyskawicznie! Pozwala to na natychmiastowe zebranie opinii na jego temat, dostosowanie go do oczekiwań i szybkie reagowanie na wymagane zmiany. Takie założenia może spełnić tylko i wyłącznie zespół wytwarzający oprogramowanie w zwinny sposób!', '978-83-246-9682-6', 'Robert C. Martin', 2010, '2014-12-08 21:09:55', '2014-12-07 21:09:55'),
(3, 'Procesy biznesowe w praktyce. Projektowanie, testowanie i optymalizacja', 'W tej książce, napisanej przez specjalistę informatyka od piętnastu lat zajmującego się procesami biznesowymi, poznasz zagadnienia dotyczące tych procesów z perspektywy praktycznej. Może ona okazać się tym cenniejsza, że autor opisuje, jak to wszystko działa w polskich warunkach, a ponadto omawia zagadnienia dotyczące zarówno przedsiębiorstw komercyjnych, jak i urzędów publicznych. Dlatego właśnie należy spodziewać się raczej porad praktycznych niż wykładu o charakterze akademickim. Miejsce naukowych definicji zajmuje praktyka zdobyta dzięki latom doświadczeń i… popełnionym przez autora błędom, na których teraz możesz się uczyć.\r\n\r\nMarek Piotrowski skupia się na czterech blokach zagadnień. Rozdziały od 2. do 5. traktują o formalnych aspektach projektowania procesów, o procesie w ogóle, o używanych notacjach i rodzajach obiegów. Następna część zawiera omówienie praktycznych zagadnień związanych z procesami biznesowymi, najczęściej popełnianymi błędami, rozwiązaniami typowych problemów spotykanych w warunkach polskich przedsiębiorstw i optymalizacją. Część trzecia mówi o testowaniu i pomiarach procesów. Ostatni, 12. rozdział mówi o tym, jakie zmiany w organizacji implikuje wprowadzenie struktury zorientowanej na procesy. ', '978-83-246-7120-5', 'Marek Piotrowski', 2011, '2014-12-07 11:25:00', NULL),
(4, 'Zarządzanie projektami. Wydanie II', '\r\nNie ryzykuj -- zastosuj skuteczne i sprawdzone narzędzia zarządzania projektami\r\n\r\nMelduję doskonałe wykonanie zadania! Skuteczne zarządzanie projektem wymaga profesjonalnego podejścia i odpowiedniego przygotowania. Aby zapewnić sukces swojemu przedsięwzięciu, potrzebujesz kilku praktycznych umiejętności. Na pewno przyda Ci się wiedza o szczegółowym planowaniu i sprawnym zarządzaniu oraz szerokie zdolności interpersonalne. Nie musisz jednak dążyć do sukcesu metodą prób i błędów. Doświadczonym praktykom, którzy na zarządzaniu projektami zjedli zęby, udało się wypracować zestaw przydatnych narzędzi, pomagających zwiększyć szansę powodzenia projektu. Materiał zawarty w tej książce prezentuje najlepsze z nich. Ponadto znajdziesz tu osobiste wskazówki autorki, opierające się na jej własnym doświadczeniu, oraz porady specjalistów, z którymi miała okazję współpracować. Pomoże Ci to poznać nie tylko teorię, lecz także aspekty praktyczne świetnego zarządzania.', '83-246-2239-X', 'Nancy Mingus', 2004, '2014-12-07 12:30:24', NULL),
(5, 'Praktyczne lekcje zarządzania projektami', ' Tak się zarządza projektami!\r\n\r\nKierowanie projektami może być naprawdę fascynujące. Wystarczy zerknąć do tej książki, aby się o tym przekonać. Oto dwanaście prawdziwych historii o przedsięwzięciach, które mimo początkowych trudności zakończyły się spektakularnym sukcesem. Rozmówcami Michała Kopczewskiego są znane osoby ze świata biznesu, sportu i życia publicznego, które podjęły się trudnych i niecodziennych zadań. Dziś dzielą się dobrymi pomysłami i lekcjami wyciągniętymi ze swoich doświadczeń. Otwarcie mówią też o potknięciach, które po drodze im się zdarzały. Masz wyjątkową okazję przyjrzeć się rozmaitym projektom: od wypraw na krańce Ziemi aż po organizację EURO 2012. Każde z opisywanych przedsięwzięć wydaje się całkiem inne, a jednak coś je łączy: wszystkie wymagały uporu i żelaznej konsekwencji.\r\n\r\nTa książka to bezcenna lektura dla każdego kierownika projektu, ale stanowić może także inspirację dla wszystkich, którzy stają przed nietypowym i skomplikowanym zadaniem. Dowodzi ona, że stosowanie dobrych praktyk zarządzania projektami to niemal gwarancja sukcesu.', '978-83-246-6770-3', 'Michał Kopczewski', 1999, '2014-12-08 07:21:41', NULL),
(6, 'Funkcjonalność informatycznych systemów zarządzania. Tom I', 'Publikacja w sposób wyczerpujący podejmuje tematykę funkcjonalności systemów informatycznych wspomagających zarządzanie (SIZ). W książce zostały szczegółowo opisane funkcje informatycznych systemów rachunkowości finansowej przeznaczonych dla małych i średnich firm, funkcje kompleksowych zintegrowanych systemów transakcyjnych stosowanych przez duże przedsiębiorstwa oraz funkcje i zastosowania uzupełniających je systemów informacyjno-analitycznych klasy Business Intelligence.Zagadnienia zawarte w tomie 1 dotyczą systemów, których głównym zadaniem jest prowadzenie ewidencji zdarzeń gospodarczych oraz wspomaganie obsługi bieżącej działalności przedsiębiorstwa, tj. systemów transakcyjnych, nazywanych też systemami ewidencyjno-operacyjnymi.', '978-8-3011-5599-5', 'Januszewski Arkadiusz', 2001, '2014-12-06 10:52:00', NULL),
(7, 'Microsoft Project 2010 krok po kroku', 'Dzięki książce Microsoft Project 2010 krok po kroku najłatwiej i najszybciej nauczysz się prowadzenia projektów za pomocą programu Microsoft Project 2010. Korzystając z książek z serii krok po kroku, samodzielnie określasz tempo nauki, opanowując i utrwalając niezbędne umiejętności wtedy, gdy ich potrzebujesz! Z książki dowiesz się, jak utworzyć plan projektu i dostosować jego szczegóły; zaplanować zadania, wyznaczyć punkty kontrolne i przydzielić zasoby; śledzić postępy pracy i koszty oraz kontrolować odchylenia od planu; rozwiązywać problemy z opóźnieniami i przekroczeniem budżetu; nauczysz się dostosowywać widoki wykresu Gantta, tabele i kalendarze a także poznasz najlepsze praktyki z zakresu zarządzania projektem. Ucząc się w odpowiednim dla siebie tempie, wykonuj ponumerowane czynności, na podstawie zrzutów ekranowych weryfikuj postępy, zapoznaj się ze wskazówkami, które zawierają przydatne informacje oraz przećwicz umiejętności, korzystając z plików ćwiczeniowych. Doskonały sposób nauki programu Microsoft Project 2010 krok po kroku!', '978-8-3724-3888-1', 'Pan Smok', 2007, '2014-12-06 06:31:00', NULL),
(8, 'Getting Things Done, czyli sztuka bezstresowej efektywności', 'Zawrotna szybkość, z jaką pędzi świat nowego milenium, niejednego z nas przyprawia o ból głowy. A gdyby tak powiedzieć STOP temu szaleństwu? Spojrzeć z boku na swoje obowiązki, piętrzące się na biurku dokumenty i skrzynkę odbiorczą, zalaną mailami? Zmniejszyć prędkość na bieżni, po której gonisz swój cel? Czy wiesz, że wystarczy zastosować spójny system zarządzania zawodowym chaosem i wirem życia prywatnego, by odzyskać spokój umysłu, a w dodatku poprawić kreatywność i efektywność?\r\n\r\nProgram Getting Things Done (GTD) powstał właśnie po to, byś mógł wreszcie zwolnić tempo, zrezygnować z przytłaczających obowiązków i uwolnić się od wiecznej presji zegarka. Jest to najbardziej spójny, intuicyjny i wydajny system racjonalizacji zadań, jak istnieje w chwili obecnej na rynku. Wyobraź sobie, że w kilka tygodni osiągasz wewnętrzną równowagę, harmonię, a co za tym idzie, lepsze wyniki. Skoro udało się to milionom wyznawców techniki GTD na całym świecie, z dużym prawdopodobieństwem możesz założyć, że uda się i Tobie! Czy nie szkoda Ci czasu, by ciągle cierpieć na jego brak?', '978-83-246-1999-3', 'David Allen', 1995, '2014-12-09 08:17:20', NULL),
(9, 'Bitcoin. Złoto XXI wieku', 'Waluta nowych, lepszych czasów\r\nBitcoin to fenomen. Mówią o nim media, sprzeczają się o niego politycy, spekulanci upatrują w nim szansy na błyskawiczne wzbogacenie się. Czasem jakaś gazeta pochyli się nad technologicznym geniuszem bitcoina, jednak znacznie częściej wybierze temat o kradzieżach i wykorzystywaniu cyfrowej waluty do nabywania narkotyków lub broni.\r\nCzym tak naprawdę jest bitcoin?\r\nNie każdy musi angażować się w społeczność bitcoina albo śledzić na bieżąco kursy jego wymiany na złotówki. Jednak każdy, kto nie chce przegapić prawdziwej rewolucji, powinien rozumieć, czym jest i w jaki sposób działa ta wirtualna waluta. Niniejsza książka to kompendium wiedzy o bitcoinie, które pomoże poznać i zrozumieć jego istotę osobom spoza branży finansowej czy branży nowoczesnych technologii.\r\n\r\nŚwiat bitcoina to dla nas wciąż nowość, mimo że w niektórych krajach ten twór funkcjonuje już jako waluta. Tak jest np. w Niemczech, w krajach skandynawskich i na Cyprze, gdzie bitcoiny można wypłacić z bankomatu. W tym tkwi piękno bitcoina - nie jest on narzucany z góry, a jego zastosowanie to kwestia wolnego wyboru. Bitcoin to jednak nie tylko rewolucja w płatnościach - to mechanizm, który może kompletnie wywrócić naszą rzeczywistość i zmienić zasady gry. To złoto XXI wieku!', '978-83-246-9748-9', 'Karol Kopańko, Mateusz Kozłowski', 1987, '2014-12-09 06:12:07', NULL),
(10, 'Analiza danych w biznesie. Sztuka podejmowania skutecznych decyzji', 'Posiadanie zbiorów danych to połowa sukcesu. Druga połowa to umiejętność ich skutecznej analizy i wyciągania wniosków. Dopiero na tej podstawie będziesz w stanie właściwie ocenić kondycję Twojej firmy oraz podjąć słuszne decyzje. Wiedza zawarta w tej książce może zadecydować o sukcesie biznesowym lub porażce. Nie ryzykuj i sięgnij po to doskonałe źródło wiedzy, poświęcone nauce o danych.\r\n\r\nTo unikalny podręcznik, który pomoże Ci sprawnie opanować nawet najtrudniejsze zagadnienia związane z analizą danych. Dowiedz się, jak zbudowany jest proces eksploracji danych, z jakich narzędzi możesz skorzystać oraz jak stworzyć model predykcyjny i dopasować go do danych. W kolejnych rozdziałach przeczytasz o tym, czym grozi nadmierne dopasowanie modelu i jak go unikać oraz jak wyciągać wnioski metodą najbliższych sąsiadów. Na koniec zaznajomisz się z możliwościami wizualizacji skuteczności modelu oraz odkryjesz związek pomiędzy nauką o danych a strategią biznesową. To obowiązkowa lektura dla wszystkich osób chcących podejmować świadome decyzje na podstawie posiadanych danych!', '978-83-246-9610-9', 'Foster Provost', 2012, '2014-12-08 00:14:12', NULL),
(11, 'Jak biegać', 'Podrecznik dla nubów', '978-83-246-2519-1', 'Michno', 2014, '2014-12-09 17:50:13', '2014-12-09 17:50:13'),
(12, 'UML', 'Podstawy UMLa - Podręcznik dla początkujących', '978-83-246-2519-1', 'Kazimierz Chodakowski', 2014, '2014-12-09 18:13:44', '2014-12-09 18:14:28');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `percent` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_discounts_users1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `percent`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '5', 2, '2014-12-07 20:26:52', '2014-12-07 20:26:52');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `permissions` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administratorzy', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Użytkownicy', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payments_users1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `user_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 20, '2014-12-09 15:09:35', '2014-12-09 15:09:35'),
(2, 22, 10, '2014-12-09 15:16:43', '2014-12-09 15:16:43'),
(3, 2, 500, '2014-12-09 15:17:53', '2014-12-09 15:17:53'),
(4, 4, 7.99, '2014-12-09 15:27:05', '2014-12-09 15:27:05');

-- --------------------------------------------------------

--
-- Table structure for table `penalties`
--

CREATE TABLE IF NOT EXISTS `penalties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_penalties_users1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `penalties`
--

INSERT INTO `penalties` (`id`, `user_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 20, '2014-12-09 14:47:20', '2014-12-09 14:47:20'),
(3, 3, 1000, '2014-12-09 14:52:03', '2014-12-09 14:52:03'),
(4, 7, 20, '2014-12-09 14:53:47', '2014-12-09 14:53:47'),
(5, 30, 10, '2014-12-09 14:57:55', '2014-12-09 14:57:55'),
(6, 26, 10, '2014-12-09 14:59:27', '2014-12-09 14:59:27'),
(7, 4, 10, '2014-12-09 15:00:53', '2014-12-09 15:00:53'),
(8, 3, 15, '2014-12-09 18:42:33', '2014-12-09 18:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `rents`
--

CREATE TABLE IF NOT EXISTS `rents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `returned_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rents_users1_idx` (`user_id`),
  KEY `fk_rents_books1_idx` (`book_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `rents`
--

INSERT INTO `rents` (`id`, `user_id`, `book_id`, `days`, `returned_at`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 3, NULL, '2014-12-09 08:12:49', '2014-12-09 08:12:49');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(6, 22, '127.0.0.1', 0, 0, 0, NULL, NULL, NULL),
(7, 1, '127.0.0.1', 0, 0, 0, NULL, NULL, NULL),
(8, 3, '127.0.0.1', 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `terms_accepted_at` mediumint(9) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) DEFAULT NULL,
  `reset_password_code` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `first_name`, `last_name`, `slug`, `activated`, `activation_code`, `activated_at`, `terms_accepted_at`, `last_login`, `persist_code`, `reset_password_code`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'dan@o2.pl', '$2y$10$R/i4CW5rZWDMhm2vupwwGuAQ1YY9qnZNOQ9IbgUXWIcTJzM7Pd74i', 'Daniel', 'Martuszewski', '', 1, NULL, NULL, NULL, '2014-12-10 08:46:50', '$2y$10$h9Sv3b6c8ah3CGdnugT11OAbSYls2XWbthGkn7zUoabfUmSeTTmc2', NULL, NULL, '2014-02-20 10:57:25', '2014-12-10 09:46:50'),
(2, 'apres@gmail.com', '$2y$10$SvTFni2ANgAhOclRas3oU.vmyI8ZINIR50VJ8Km04VkFCKhfK9udC', 'Agn', 'Pres', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-02-20 10:57:25', '2014-05-18 10:50:25'),
(3, 'user@o2.pl', '$2y$10$AtAwwUWipolyZQOF/CpTTOxnNtbN7dIytZJ4s3GPTZ8naVo53HPBa', 'Piotr', 'Matras', '', 1, NULL, NULL, NULL, '2014-12-09 17:49:01', '$2y$10$1D.CLq6uedZME42HLa46WOsEpFlaA4W7CbWIdVHGxJ0mvbj4SU7tC', NULL, NULL, '2014-02-20 10:57:25', '2014-12-09 18:49:01'),
(4, 'boss@matras.info.pl', '$2y$10$5AdmZRTVK25VJv9lgrRHIeprIWfJaakj1XBO19iSeuulTYkqreXWS', 'Joe', 'Mat', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-02-20 10:57:25', '2014-02-20 12:24:48'),
(7, 'john.doe@example.com', '$2y$10$0JDxXbLUnNN.7Wn2wSsNM.Q6gCxtwE6SVF5ktmLImDd.p48a9Pop6', 'Joe', 'Doe', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-02-24 08:35:42', '2014-05-18 09:58:42'),
(8, 'dad@dad.pl', '$2y$10$44yNCO5Jpe0B8Kt1BixSt..0muD/pbtWmcy9leUgC83GLMjIM65ym', 'Dan', 'Bilzerian', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-02-24 09:03:42', '2014-02-24 09:03:42'),
(22, 'qaztest4@gmail.com', '$2y$10$.P10adbmyGYXSAodCDZ.yOTjxMS2TX.fFDgnw.E4kxdFT1bBT3Qia', 'Konrad', 'Łata', '', 1, NULL, NULL, NULL, '2014-12-10 09:10:25', '$2y$10$iAoxc6m297ZPCBES65My5u./ZC6Phi1HIYqAMofc/zchquJAiKwt6', NULL, NULL, '2014-02-26 07:45:50', '2014-12-10 10:10:25'),
(23, 'provider@o2.pl', '$2y$10$l0BwaU5ZW3Ct75n3ilnsDuHMPL2BvoimYCo44WhURym06t/27SuB.', 'Calvin', 'Harris', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-03-07 12:52:57', '2014-03-11 09:18:22'),
(24, 'v3d@o2.pl', '$2y$10$jubdrABs3yqaMFf0E8npL.w5QzGu0hSPT.fFXwk0QAR6T/amASeFy', 'Zofia', 'Bielczuk', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-03-07 13:05:16', '2014-05-18 16:57:50'),
(26, 'dmartuszewski.screenshots2@gmail.com', '$2y$10$WiRwVeWk9ouCp8Hkg.CLlOlnkM0KRpoLHCtT.og8ZvGOAQL6J5Ab.', 'Pan', 'Michno', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-05-19 11:06:46', '2014-05-19 11:24:06'),
(30, 'dmartuszewski.screenshots@gmail.com', '$2y$10$SoumLQMTORXhLBLkZR1I7.eedRoVB2R9J3zEU9JklHV0wzN0KC8Yi', 'David', 'Hasselhof', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-05-19 12:13:29', '2014-05-19 12:13:44'),
(31, 'fakapyrer@gmail.com', '$2y$10$4liHlS8M.Aj0aTPUxTgyMOPofU0dLVJlI.x3ECn0cbFlkrpp3CKk2', 'Jarrod', 'Newton', '', 0, '81i0w1QIha9kZ5ZdIF1OnbNbgTuFpUr0piLwbB6Lmn', NULL, NULL, NULL, NULL, NULL, NULL, '2014-12-09 18:55:24', '2014-12-09 18:55:24'),
(32, 'dan+12@o2.pl', '$2y$10$wbuZmbACwSpGpVIPtcxUDOgGXVGLK3bsrPj0v4VJIg1oOc2/j8wZC', 'Dan', 'Ma', '', 0, 'ejFVURk6axsmfV3y0USydZ3QMH8OpFFlBRpPKe1Xt9', NULL, NULL, NULL, NULL, NULL, NULL, '2014-12-10 09:50:38', '2014-12-10 09:50:38'),
(33, 'dan+122@o2.pl', '$2y$10$7pgiIr8nQk/SqataiswIB.zAqVzucwm0GXEg3F0tRFiGWRD4ndvS2', 'Dan', 'Ma', '', 0, 'tvIysP932LB1Al0KlgdPKiJMJk9WWpjJF8a7SKwzU0', NULL, NULL, NULL, NULL, NULL, NULL, '2014-12-10 09:52:33', '2014-12-10 09:52:33'),
(34, 'dan+124@o2.pl', '$2y$10$BsVffYOcwn.sj/BHFwXvzu03x7mh1FntQCESQI98Mq42mYFrb5qAC', 'Dan', 'Ma', '', 0, 'M2v4qsrlyv60y5uz4pl81pZqdak3dxfxdk1pnwYZuH', NULL, NULL, NULL, NULL, NULL, NULL, '2014-12-10 09:53:12', '2014-12-10 09:53:12'),
(35, 'dan+134@o2.pl', '$2y$10$zZ3Lo6I9ywb1sv3x/oMV7uW9z.P2LYnTiYDBhnxAPdJm7uw/D.WkO', 'Dan', 'Ma', '', 0, '3Zc3xNV4Qd86mkur4Rh5z6D4SBY9oTDYvrvFQsF3TL', NULL, NULL, NULL, NULL, NULL, NULL, '2014-12-10 09:54:54', '2014-12-10 09:54:54'),
(36, 'dan+1344@o2.pl', '$2y$10$9vcbDBKVT92g/O5dbTxet.txgcn8KISekMFRRAuIok/97OeYuYjmC', 'Dan', 'Ma', '', 0, 'ZZ2CakkOxoqVMstClREz2vbj3EtQbztfpCbOxzdc3z', NULL, NULL, NULL, NULL, NULL, NULL, '2014-12-10 09:55:40', '2014-12-10 09:55:40'),
(37, 'dan+13214@o2.pl', '$2y$10$wnJaB4u9nqJYnc3B8c9vLO1SxCXjUran6mW4SHlPjeMEzJiXoP/SW', 'Dan', 'Ma', '', 1, 'RetapRYgWHZlYMk9RkyAJaAnCDpG2umtVPmlGk2rau', NULL, 2014, NULL, NULL, NULL, NULL, '2014-12-10 10:04:10', '2014-12-10 10:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(7, 1),
(3, 2),
(4, 2),
(8, 2),
(23, 2),
(24, 2),
(26, 2),
(30, 2),
(2, 2),
(22, 2),
(22, 1),
(1, 1),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `discounts`
--
ALTER TABLE `discounts`
  ADD CONSTRAINT `fk_discounts_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `fk_payments_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `penalties`
--
ALTER TABLE `penalties`
  ADD CONSTRAINT `fk_penalties_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rents`
--
ALTER TABLE `rents`
  ADD CONSTRAINT `fk_rents_books1` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rents_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
