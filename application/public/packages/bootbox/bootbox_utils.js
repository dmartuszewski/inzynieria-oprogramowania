function yesNo(title, message, successCallback) {
    bootbox.dialog({
        message: message || "Are you sure?",
        title: title || "Confirm",
        buttons: {
            yes: {
                label: "Tak",
                className: "btn-success",
                callback: successCallback
            },
            no: {
                label: "Nie",
                className: "btn-danger",
                callback: function() {
                    res = false;
                }
            }
        }
    });
}