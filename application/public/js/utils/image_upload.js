function uploadPreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var $preview = $('#' + $(input).attr('id') + '-preview');
            $preview.css('height', $preview.height()).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}