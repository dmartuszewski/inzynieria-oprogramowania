$(window).resize(stickUnstickFooter);

$(document).ready(function() {

    stickUnstickFooter();

});

lastResizingHeight = $(window).outerHeight();
function stickUnstickFooter() {
    var windowH   = $(window).outerHeight(),
        direction = lastResizingHeight - windowH > 0; // true - up, false - down

    lastResizingHeight = windowH;

    if( (windowH <= $('html').height() && !direction) || (windowH <= $('html').height() + $('footer').outerHeight() && direction)) {
        $('footer').removeClass('navbar-fixed-bottom');
    } else {
        $('footer').addClass('navbar-fixed-bottom');
    }
}