$(document).ready(function () {


    bindHover('.check-task');
    bindClick('.check-task');


    $('#task-form').on('submit', function(e) {
        e.preventDefault();

        var $this = $(this);

        $('.task-adding-error').remove();

        $.ajax({
            url: $this.attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(d) {

                if(d.status == 'success') {
                    $('#tasks-list').prepend( $.tmpl($('#template-new-task'), d) );
                    $('.new-task').fadeIn();

                    $('#task').val('');

                    bindHover('.new-task');
                    bindClick('.new-task a');

                } else if(d.status == 'error') {

                    $('#tasks').prepend($('<p/>', {class: 'alert alert-danger task-adding-error', text: d.message}));

                }
            }
        });

    });
});

function bindClick(elem) {
    $(elem).on('click', function(e){
        e.preventDefault();
        var $this = $(this);

        if($this.hasClass('task-complete')) {

            $('i', this).attr('class', 'glyphicon glyphicon-unchecked');
            $this.removeClass('task-complete');
            $this.attr('title', 'Mark as Complete');
            $this.siblings('span').fadeOut();

            $.post('/incomplete-task/' + $this.data('slug'));
        } else {
            $('i', this).attr('class', 'glyphicon glyphicon-check');
            $this.addClass('task-complete');
            $this.attr('title', 'Mark as Incomplete');

            $.post('/complete-task/' + $this.data('slug'));
            $this.parent().append($('<span/>', {class: 'text-muted h complete-msg', text: 'Completed by you right now'}));
            $('.complete-msg').fadeIn('fast', function(){
                $(this).removeClass('complete-msg');
            });
        }

    });
}

function bindHover(elem) {
    $(elem + ' i').hover(function(){
        if(!$(this).parent().hasClass('task-complete')) {
            $(this).attr('class', 'glyphicon glyphicon-check');
        }
    }, function() {
        if(!$(this).parent().hasClass('task-complete')) {
            $(this).attr('class', 'glyphicon glyphicon-unchecked');
        }
    });
}