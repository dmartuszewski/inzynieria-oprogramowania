$(document).ready(function () {
    $('.confirm-return').on('click', function(e) {
        e.preventDefault();
        var $form = $(this).parents('form:first-child');

        yesNo('Potwierdź oddanie ksiązki', 'Czy na pewno chcesz zapisać oddanie ksiązki?', function() {
            $form.submit();
        });

    });
});