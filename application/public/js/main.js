var datetimepickerOpts = {
    language: 'pl',
    pick12HourFormat: false,
    minuteStepping: 5,
    format: 'YYYY-MM-DD HH:mm'
}
/**
 * Function cares about dynamic layouts elements like fixed footer
 * Call it each time you change html height
 */
function refreshLayout() {
    if( typeof stickUnstickFooter == "function") {
        stickUnstickFooter();
    }
}

$(document).ready(function () {
    $('.remove-submit').on('click', function(e) {
        e.preventDefault();

        var $t = $(this);

        yesNo('Potwierdź akcję', 'Czy na pewno chcesz usunąć?', function() {
            $t.parent().submit();
        });
    });

    $('[data-toggle="popover"]').popover({
        trigger: 'hover'
    });
});