<?php

class UsersController extends BaseController {

    /**
     * User Repository
     *
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = $this->user->paginate($this->paginationLimit);

        return View::make('users.index', compact('users'));
    }

    public function getRegister()
    {
        return View::make('users.register');
    }

    public function postRegister()
    {
        $inputs = Input::all();

        $user = $this->user->register($inputs);
        if ( !$user )
        {
            $this->flashFailure('Sprawdź poprawność danych!');
            return Redirect::to('register')->withInput()->withErrors($this->user->getErrors());
        }

        Session::put('credentials', ['email' => $user->email, 'password' => $inputs['password']]);
        Auth::once(Session::get('credentials'));

		$user = Auth::user();
		$user->activated = 1;
		$user->save();

        return Redirect::route('accept_terms');
    }

    public function getTerms()
    {
        Auth::once(Session::get('credentials'));
        return View::make('users.terms');
    }

    public function postTerms()
    {
        Auth::once(Session::get('credentials'));

        if( ! Input::get('terms_accepted_at') )
        {
            return Redirect::route('accept_terms')->withInput()->withErrors([
                'terms_accepted_at' => 'Zaakceptuj regulamin.'
            ]);
        }

        $user = Auth::user();
        $user->terms_accepted_at = date('Y-m-d H:i:s');

        Auth::login(Auth::user());

        if($user->save())
        {
            $this->flashSuccess('Konto zostało utworzone. Potwierdź rejestrację klikając w link, który został wysłany na podany adres e-mail.');
            return Redirect::route('login');
        }

        $this->flashFailure('Błąd, spróbuj jeszcze raz!');
        return Redirect::route('accept_terms')->withInput()->withErrors([
            'terms_accepted_at' => 'Zaakceptuj regulamin.'
        ]);
    }

    public function activate($code)
    {

        $user = User::whereActivationCode($code)->first();

        // Attempt to activate the user
        if ($user && $user->attemptActivation($code))
        {
            $this->flashSuccess('Twoje konto jest aktywne. Witaj w Localeo!');
        }
        else
        {
            $this->flashFailure('Podany kod jest nieprawidłowy lub konto już zostało potwierdzone wcześniej. W razie problemów skontaktuj się z administratorem strony.');
        }

        if(Auth::check())
        {
            return Redirect::to('/');
        }

        return Redirect::route('login');
    }


    public function getLogin()
    {
        if(Auth::check())
        {
            $this->flashWarning(sprintf('Jesteś zalogowany.'));
            return Redirect::to('/');
        }

        return View::make('users.login');
    }

    public function postLogin()
    {

        $error = false;
        try
        {
            $user = Sentry::authenticate([
                'email'    => Input::get('email'),
                'password' => Input::get('password'),
            ]);

            Auth::attempt([
                'email'    => Input::get('email'),
                'password' => Input::get('password'),
            ]);
        }
        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            $error = 'Podaj login.';
        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            $error = 'Podaj hasło.';
        }
        catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
        {
            $error = 'Złe hasło, spróbuj jeszcze raz.';
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            $error = 'Brak konta o podanym loginie.';
        }
        catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
        {
            $error = 'Konto nie zostało potwierdzone.';
        }
        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
        {
            $error = 'Konto zawieszone.';
        }
        catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
        {
            $error = 'Konto zablokowane.';
        }

        if($error)
        {
            return View::make('users.login')->with('error', $error);
        }

        $this->flashSuccess(sprintf('Witaj %s. Miło Cię znowu widzieć!', $user->first_name));
        return Redirect::intended('/');
    }

    public function getLogout()
    {
        Auth::logout();
        $this->flashSuccess('Do zobaczenia wkrótce!');
        // Redirect to blog will be nice thing!
        return Redirect::route('/');
    }

    public function getRestorePasswordRequest()
    {
        return View::make('users.restore_password_request');
    }

    public function postRestorePasswordRequest()
    {
        try
        {
            // Find the user using the user email address
            $user = Sentry::findUserByLogin(Input::get('email'));

            $user->sendResetPasswordCode();

            $this->flashSuccess('Na adres e-mail zostały wysłany link do zmiany hasła. Postępuj zgodnie z instrukcjami.');
            return Redirect::route('/');
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return View::make('users.restore_password_request')->with('error', 'Konto o podanym adresie e-mail nie istnieje.');
        }
    }


    public function getResetPassword($code)
    {
        return View::make('users.reset_password', ['resetPasswordCode' => $code]);
    }

    public function postResetPassword()
    {

        try
        {

            $user = Sentry::findUserByLogin(Input::get('email'));

            // Validates new password
            $v = Validator::make(Input::all(), ['password' => $this->user->rules['password']]);

            if($v->fails())
            {
                throw new Exception($v->errors()->get('password')[0]);
            }

            $invalidCode = 'Podany link do zmiany hasła jest nieprawidłowy. Spróbuj jeszcze raz kliknąć w link.';

            // Check if the reset password code is valid
            if ($user->checkResetPasswordCode(Input::get('password_reset_code')))
            {
                // Attempt to reset the user password
                if ($user->attemptResetPassword(Input::get('password_reset_code'), Input::get('password')))
                {
                    $this->flashSuccess('Hasło zostało zmienione.');
                    return Redirect::route('login');
                }
                else
                {
                    // Password reset failed
                    $this->flashFailure($invalidCode);
                }
            }
            else
            {
                $this->flashFailure($invalidCode);
            }

            return Redirect::to('/');
        }
        catch (Exception $e)
        {
            return View::make('users.reset_password', [
                'resetPasswordCode' => Input::get('password_reset_code'),
                'error' => $e->getMessage()
            ]);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->user->findOrFail($id);

        return View::make('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id = null)
    {
        if(!$id)
        {
            $user = Auth::user();
        }
        else
        {
            $user = $this->user->find($id);
        }

        if (is_null($user))
        {
            return Redirect::route('users.ro');
        }

        $userGroups = [];
        foreach($user->getGroups() as $g)
        {
            $userGroups[] = $g->id;
        }
        $groups = Group::lists('name', 'id');

        return View::make('users.edit', compact('user', 'groups', 'userGroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id = null)
    {
        if(!$id)
        {
            $id = Auth::user()->id;
        }
        $input = array_except(Input::all(), '_method');
        $rules = ['first_name' => User::$rules['first_name'], 'last_name' => User::$rules['last_name']];
        $validation = Validator::make($input, $rules);

        if ($validation->passes())
        {
            $newPass = Input::get('password');
            $sentryUser = Sentry::getUserProvider()->findById($id);
            if(!empty($newPass) )
            {
                $sentryUser->password = $newPass;

            }

            foreach(Sentry::findAllGroups() as $group)
            {
                $sentryUser->removeGroup($group);
            }

            foreach(Input::get('groups') as $groupId)
            {
                $g = Sentry::findGroupById($groupId);
                $sentryUser->addGroup($g);
            }
            $sentryUser->save();

            $user = $this->user->find($id);
            $user->update($input);

            return Redirect::route('users.show', $id);
        }

        return Redirect::route('users.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'Problem z walidacją.');
    }


    public function getSettings()
    {
        $user = Auth::user();
        return View::make('users.settings', compact('user'));
    }

    public function postSettings()
    {
        $id = Auth::user()->id;
        $input = array_except(Input::all(), '_method');
        $rules = ['first_name' => User::$rules['first_name'], 'last_name' => User::$rules['last_name']];
        $validation = Validator::make($input, $rules);

        if ($validation->passes())
        {
            $user = $this->user->find($id);

            $newPass = Input::get('password');
            if( !empty($newPass) )
            {
                $sentryUser = Sentry::getUserProvider()->findById($id);
                $sentryUser->password = $newPass;
                $sentryUser->save();
            }

            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');

            if($user->save())
            {
                $this->flashSuccess('Ustawienia zostały zapisane');
                return Redirect::route('user_reservations');
            }
            else
            {
                $this->flashFailure('Wystąpił błąd, spróbuj ponownie');
            }
        }

        return Redirect::back()
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'Formularz zawiera błędy.');
    }

}
