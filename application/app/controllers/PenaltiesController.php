<?php

class PenaltiesController extends BaseController {
	/**
	* Book Repository
	*
	* @var Book
	*/
	protected $penalty;

	public function __construct(Penalty $penalty)
	{
		$this->penalty = $penalty;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$penalties = Penalty::orderBy('created_at', 'desc')->paginate($this->paginationLimit);
        return View::make('penalties.index', compact('penalties'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $users = $this->getUsersList();

        return View::make('penalties.create', compact('users'));
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Penalty::$rules);

		if ($validation->passes())
		{
			$p = $this->penalty->create($input);

			$this->flashSuccess(sprintf('Kara %d PLN została przydzielona', $input['amount']));

			return Redirect::route('penalties.show', $p->id);
		}

		return Redirect::route('penalites.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$penalty = $this->penalty->findOrFail($id);

		return View::make('penalties.show', compact('penalty'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$penalty = $this->penalty->find($id);

		if (is_null($penalty))
		{
			return Redirect::route('penalties.index');
		}

		$users = $this->getUsersList();

		return View::make('penalties.edit', compact('penalty', 'users'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Penalty::$rules);

		if ($validation->passes())
		{
			$penalty = $this->penalty->find($id);
			$penalty->update($input);

			return Redirect::route('penalties.show', $id);
		}

		return Redirect::route('penalties.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->penalty->find($id)->delete();

		$this->flashSuccess('Usunięto');

		return Redirect::route('penalties.index');
	}

}
