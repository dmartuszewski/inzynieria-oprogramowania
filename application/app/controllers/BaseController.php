<?php

class BaseController extends Controller {

    protected $layout = 'layouts.master';
    protected $paginationLimit = 10;

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{

		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    private function setSessionFlash($msg, $type)
    {
        Session::flash('flash', array(
            'message' => $msg,
            'type' => $type,
        ));
    }

    protected function flashSuccess($msg)
    {
        $this->setSessionFlash($msg, 'success');
    }

    protected function flashWarning($msg)
    {
        $this->setSessionFlash($msg, 'warning');
    }

    protected function flashFailure($msg)
    {
        $this->setSessionFlash($msg, 'danger');
    }

	protected function getUsersList()
	{
		$objs = Sentry::findAllUsersInGroup(Sentry::findGroupById(2));

		$users = [];

		foreach($objs as $user)
		{
			$users[$user->id] = $user->name();
		}

		return $users;
	}
}