<?php

class DiscountsController extends BaseController {

	/**
	 * Discount Repository
	 *
	 * @var Discount
	 */
	protected $discount;

	public function __construct(Discount $discount)
	{
		$this->discount = $discount;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$discounts = Discount::paginate($this->paginationLimit);
        return View::make('discounts.index', compact('discounts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$users = $this->getUsersList();

        return View::make('discounts.create', compact('users'));
	}

	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Discount::$rules);

		if ($validation->passes())
		{
			$p = $this->discount->create($input);

			$this->flashSuccess(sprintf('Zniżka %s%% została przypisana', $input['percent']));

			return Redirect::route('discounts.show', $p->id);
		}

		return Redirect::route('discounts.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$discount = $this->discount->findOrFail($id);

		return View::make('discounts.show', compact('discount'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$discount = $this->discount->find($id);

		if (is_null($discount))
		{
			return Redirect::route('discounts.index');
		}
		$users = $this->getUsersList();

		return View::make('discounts.edit', compact('discount', 'users'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Discount::$rules);

		if ($validation->passes())
		{
			$discount = $this->discount->find($id);
			$discount->update($input);

			return Redirect::route('discounts.show', $id);
		}

		return Redirect::route('discounts.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->discount->find($id)->delete();

		$this->flashSuccess('Usunięto');

		return Redirect::route('discounts.index');
	}

}
