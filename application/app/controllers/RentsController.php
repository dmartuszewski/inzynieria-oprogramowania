<?php

class RentsController extends BaseController {

	/**
	 * Rent Repository
	 *
	 * @var Rent
	 */
	protected $rent;

	public function __construct(Rent $rent)
	{
		$this->rent = $rent;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$rents = Rent::orderBy('id', 'desc')->paginate($this->paginationLimit);

        return View::make('rents.index', compact('rents'));
	}


	public function userIndex()
	{
		$rents = Rent::orderBy('id', 'desc')->ofCurrentUser()->paginate($this->paginationLimit);

        return View::make('rents.user_index', compact('rents'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param $bookId
	 *
	 * @return Response
	 */
	public function create($bookId)
	{
		$book = Book::findOrFail($bookId);

        return View::make('rents.create', compact('book'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Rent::$rules);

		if ($validation->passes())
		{
			$input['user_id'] = Auth::user()->id;
			$this->rent->create($input);

			return Redirect::route('user_rents');
		}

		return Redirect::route('rents.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$rent = $this->rent->findOrFail($id);

		return View::make('rents.show', compact('rent'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$rent = $this->rent->find($id);

		if (is_null($rent))
		{
			return Redirect::route('rents.index');
		}

		return View::make('rents.edit', compact('rent'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Rent::$rules);

		if ($validation->passes())
		{
			$payment = $this->rent->find($id);
			$payment->update($input);

			return Redirect::route('rents.show', $id);
		}

		return Redirect::route('rents.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->rent->find($id)->delete();

		$this->flashSuccess('Usunięto');

		return Redirect::route('rents.index');
	}

	public function end()
	{
		$rent = $this->rent->findOrFail(Input::get('id'));

		if($rent->end())
		{
			$this->flashSuccess('Ksiązka została oznaczona jako oddana.');
		}
		else
		{
			$this->flashFailure('Zmiany nie zostały zapisane, spóbuj ponownie.');
		}

		return Redirect::route('rents.index');
	}

}
