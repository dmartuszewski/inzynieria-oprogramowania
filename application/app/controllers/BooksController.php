<?php

class BooksController extends BaseController
{
	/**
	 * Book Repository
	 *
	 * @var Book
	 */
	protected $book;

	public function __construct(Book $book)
	{
		$this->book = $book;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$books = Book::paginate($this->paginationLimit);
		return View::make('books.index', compact('books'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function latest()
	{
		$books = Book::orderBy('created_at', 'desc')->where('created_at', '>=', date('Y-m-d', strtotime('2 days ago')))->paginate($this->paginationLimit);
		$current = 'latest_books';
		return View::make('books.user_index', compact('books', 'current'));
	}

	public function search()
	{
		$input = Input::all();

		$q = Book::orderBy('created_at', 'desc');

		foreach(['title', 'description'] as $field)
		{
			if(array_key_exists($field, $input))
			{
				$q->where(DB::raw("LOWER({$field})"), 'LIKE', "%" . strtolower($input[$field]) . "%");
			}
		}

		$books =$q->paginate($this->paginationLimit);

		$current = 'books_search';

		return View::make('books.user_index', compact('books', 'current'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('books.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Book::$rules);

		if ($validation->passes())
		{
			$p=$this->book->create($input);

			return Redirect::route('books.show', $p->id);
		}

		return Redirect::route('books.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$book = $this->book->findOrFail($id);

		return View::make('books.show', compact('book'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$book = $this->book->find($id);

		if (is_null($book))
		{
			return Redirect::route('books.index');
		}

		return View::make('books.edit', compact('book'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Book::$rules);

		if ($validation->passes())
		{
			$book = $this->book->find($id);
			$book->update($input);

			return Redirect::route('books.show', $id);
		}

		return Redirect::route('books.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->book->find($id)->delete();

		$this->flashSuccess('Książka została usunięta');

		return Redirect::route('books.index');
	}

}
