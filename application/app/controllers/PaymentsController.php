<?php

class PaymentsController extends BaseController {

	/**
	 * Book Repository
	 *
	 * @var Book
	 */
	protected $payment;

	public function __construct(Payment $payment)
	{
		$this->payment = $payment;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$payments = Payment::orderBy('created_at', 'desc')->paginate($this->paginationLimit);
        return View::make('payments.index', compact('payments'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$users = $this->getUsersList();

        return View::make('payments.create', compact('users'));

	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Payment::$rules);

		if ($validation->passes())
		{
			$p = $this->payment->create($input);
			$this->flashSuccess('Płatność została zapisana');

			return Redirect::route('payments.show', $p->id);
		}

		return Redirect::route('payments.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$payment = $this->payment->findOrFail($id);

		return View::make('payments.show', compact('payment'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$payment = $this->payment->find($id);

		if (is_null($payment))
		{
			return Redirect::route('payments.index');
		}
                $users = User::lists(DB::raw('CONCAT(first_name, " ", last_name)'), 'id');

		return View::make('payments.edit', compact('payment', 'users'));

		$users = $this->getUsersList();

		return View::make('payments.edit', compact('payment', 'users'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Payment::$rules);

		if ($validation->passes())
		{
			$payment = $this->payment->find($id);
			$payment->update($input);

			return Redirect::route('payments.show', $id);
		}

		return Redirect::route('payments.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'Formularz zawiera błędy.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->payment->find($id)->delete();

		$this->flashSuccess('Usunięto');

		return Redirect::route('payments.index');
	}

}
