<?php

Validator::extend('alpha_space', function($attr, $value) {
    return preg_match('/^([a-z\x20])+$/i', $value);
});

Validator::extend('numeric_dash', function($attr, $value) {
    return preg_match('/^([0-9\-])+$/i', $value);
});

Validator::extend('alpha_ext', function($attr, $value) {
    $allowed = array(".", ",", ":", "-", "_", "@", "?", "!", "%", " ", "\n");
    return ctype_alnum(str_replace($allowed, '', $value));
});

Validator::extend('numeric_plus', function($attr, $value) {
    return $value > 0;
});

/**
 * Params
 *  -resolution ex: 700x500 (width x height)
 */
Validator::extend('max_resolution', function($attr, $value, $params) {
    list($allowedWidth, $allowedHeight) = explode('x', $params[0]);
    list($width, $height) = getimagesize($value->getPathname());

    return $width <= $allowedWidth && $height <= $allowedHeight;
});

Validator::extend('reserved', function($attr, $value, $params) {
    $s = Input::get('start');
    $e = Input::get('end');

    $n = Reservation::where(function($q) use ($s, $e) {
        $q->where('start', '<=', $s);
        $q->where('end', '>=', $e);
    })
    ->orWhere(function($q) use ($s, $e) {
        $q->where('start', '>=', $s);
        $q->where('start', '<=', $e);
    })
    ->orWhere(function($q) use ($s, $e) {
        $q->where('start', '<=', $s);
        $q->where('end', '>=', $s);
    })->count();

    return !$n;
});