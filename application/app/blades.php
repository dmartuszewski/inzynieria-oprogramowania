<?php

// @placeholder('50x50')
Blade::extend(function($view) {
    $pattern = '/@placeholder\(\s?[\'\"](.*)[\'\"]\s?\)/';
    $replacement = '<img src="/images/placeholders/$1_cmx_blank.png"/>';

    return preg_replace($pattern, $replacement, $view);
});
// @placeholderSrc('50x50')
Blade::extend(function($view) {
    $pattern = '/@placeholderSrc\(\s?[\'\"](.*)[\'\"]\s?\)/';
    $replacement = '/images/placeholders/$1_cmx_blank.png';

    return preg_replace($pattern, $replacement, $view);
});
// @nothingFound('You have not started any project yet')
Blade::extend(function($view) {
    $pattern = '/@nothingFound\(\s?[\'\"](.*)[\'\"]\s?\)/';
    $replacement = '<p class="alert alert-info">$1</p>';

    return preg_replace($pattern, $replacement, $view);
});
// @nothingFound
Blade::extend(function($view) {
    $pattern = '/@nothingFound/';
    $replacement = '<p class="alert alert-info">Nothing Found</p>';

    return preg_replace($pattern, $replacement, $view);
});