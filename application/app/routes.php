<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as' => '/', 'uses' => 'PagesController@showHome']);
Route::get('register', ['as' => 'register', 'uses' => 'UsersController@getRegister']);
Route::post('register', ['as' => 'register', 'uses' => 'UsersController@postRegister']);
Route::get('accept-terms', ['as' => 'accept_terms', 'uses' => 'UsersController@getTerms']);
Route::post('accept-terms', ['as' => 'accept_terms', 'uses' => 'UsersController@postTerms']);
Route::get('activate-account/{activationCode}', ['as' => 'activate_account', 'uses' => 'UsersController@activate']);


Route::get('login', ['as' => 'login', 'uses' => 'UsersController@getLogin']);
Route::post('login', ['as' => 'login', 'uses' => 'UsersController@postLogin']);
Route::get('password-restore-request', ['as' => 'password_restore_request', 'uses' => 'UsersController@getRestorePasswordRequest']);
Route::post('password-restore-request', ['as' => 'password_restore_request', 'uses' => 'UsersController@postRestorePasswordRequest']);
Route::post('reset-password', ['as' => 'reset_password', 'uses' => 'UsersController@postResetPassword']);
Route::get('reset-password/{code}', ['as' => 'reset_password', 'uses' => 'UsersController@getResetPassword']);

Route::group(array('before' => 'auth'), function()
{
    Route::get('logout', ['as' => 'logout', 'uses' => 'UsersController@getLogout']);

    Route::get('ustawienia-konta', ['as' => 'settings', 'uses' => 'UsersController@getSettings']);
    Route::post('ustawienia-konta', ['as' => 'settings', 'uses' => 'UsersController@postSettings']);
    Route::post('ustawienia-konta', ['as' => 'settings', 'uses' => 'UsersController@postSettings']);

    Route::group(array('before' => 'user'), function() {
        Route::get('search', ['as' => 'books_search', 'uses' => 'BooksController@search']);
		Route::get('latest', ['as' => 'latest_books', 'uses' => 'BooksController@latest']);
		Route::get('my-rents', ['as' => 'user_rents', 'uses' => 'RentsController@userIndex']);
		Route::get('new-rent/{bookId}', ['as' => 'new_rent', 'uses' => 'RentsController@create']);
		Route::post('new-rent/{bookId}', ['as' => 'new_rent', 'uses' => 'RentsController@store']);
    });

    Route::group(array('before' => 'admin'), function() {
		Route::resource('books', 'BooksController');
		Route::resource('penalties', 'PenaltiesController');
		Route::resource('discounts', 'DiscountsController');
		Route::resource('payments', 'PaymentsController');
		Route::resource('rents', 'RentsController');
        Route::resource('users', 'UsersController');

		Route::post('end-rent', ['as' => 'end_rent', 'uses' => 'RentsController@end']);
    });
});

