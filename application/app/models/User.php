<?php

use Hampel\Sentry\Auth\SentryUser;

/**
 * Class User
 */
class User extends SentryUser {

    /**
     * Fields fillable on bulk save.
     *
     * @var array
     */
    protected  $fillable = ['first_name', 'last_name', 'email', 'password'];

    /**
     * Use soft delete.
     *
     * @var bool
     */
    protected $softDelete = true;

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required|alpha|min:2|max:20',
        'last_name' => 'required|alpha|min:2|max:20',
        'email' => 'required|email|unique:users',
        'password' => 'required_without:id|confirmed|min:8|max:50',
    ];

	/**
	 * Validation errors.
	 *
	 * @var array
	 */
	private $errors;

	/**
	 * Discount relation.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function discounts()
	{
		return $this->hasMany('Discount');
	}

	/**
	 * Rent relation.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function rents()
	{
		return $this->hasMany('Rent');
	}

	/**
	 * Full name.
	 *
	 * @return string
	 */
	public function name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

	/**
	 * Returns validation errors.
	 *
	 * @return array
	 */
	public function getErrors()
    {
        return $this->errors;
    }

	/**
	 * Register user.
	 *
	 * @param array $data
	 *
	 * @return User User object
	 */
	public function register($data)
    {
        $v = Validator::make($data, self::$rules);

        if ($v->fails())
        {
            $this->errors = $v->errors();
            return false;
        }

        // Create the user
        $user = Sentry::createUser(array(
            'slug'       => $this->generateSlug($data),
            'first_name' => $data['first_name'],
            'last_name'  => $data['last_name'],
            'email'      => $data['email'],
            'password'   => $data['password'],
            'activated'  => false
        ));

        $usersGroup = Sentry::findGroupById(2);

        $user->addGroup($usersGroup);

        $user->sendActivationCode();

        return $user;
    }

	/**
	 * Generates unique slug for user.
	 *
	 * Bases on first and last name.
	 *
	 * @param $data
	 *
	 * @return string
	 */
	protected function generateSlug($data)
    {
        $i = 0;
        do
        {
            $slug = Str::slug($data['first_name'][0] . $data['last_name']);

            if($i > 0)
            {
                $slug .= '_' . $i;
            }

            ++$i;
        }
        while( $this->where('slug', '=', $slug)->count() != 0);

        return $slug;
    }

	/**
	 * Send email with activation code to user.
	 */
	public function sendActivationCode()
    {
        // to use in closure, cannot use lexical variable ($this)
        $user = $this->toArray();
        // activation_code is hidden in SentryUser model, so we add this manually
        $user['activation_code'] = $this->getActivationCode();
        $user['name'] = $this->name();

        Mail::later(5, 'users.emails.registration_confirm', $user, function($message) use ($user)
        {
            $message->to($user['email'], $user['name'])->subject('Potwierdź e-mail w serwisie Localeo');
        });

    }

	/**
	 * Send password reset code.
	 */
	public function sendResetPasswordCode()
    {
        // Generate new activation code
        $code = substr(md5(time() . uniqid()), 7, 34);
        $this->reset_password_code = $code;
        $this->save();

        // to use in closure, cannot use lexical variable ($this)
        $user = $this->toArray();
        // rese_code is hidden in SentryUser model, so we add this manually
        $user['reset_password_code'] = $code;
        $user['name'] = $this->name();

        Mail::later(5, 'users.emails.restore_password', $user, function($message) use ($user)
        {
            $message->to($user['email'], $user['name'])->subject('Odzyskaj hasło.');
        });

    }

	/**
	 * Mark user as deleted.
	 *
	 * @return bool
	 */
	public function delete()
    {
        $this->delete();
        return true;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }
}