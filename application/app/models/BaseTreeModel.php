<?php

class BaseTreeModel extends BaseModel {

    // this is recursive function
    public function treeBuild($level = 0)
    {

        $cate = with(implode('.', array_fill(null, $level, 'children')))->whereParentId(null)->get();

        foreach ($cate as $list) {

            // this is look ugly for reset level when root category found ( top parent_id must = 0 )
            if ($list->parent_id != null)
            {
                $level++;
            }
            else
            {
                // reset level
                $level = 0;
            }
            // add  " -  " each level depth
            $dat = str_repeat("- ", $level);
            $tree[$list->id] = $dat . $list->name;

            // check if this category has children
            if (!is_null($list->children))
            {
                $level++;

                foreach ($list->children as $lists)
                {
                    // add  " -  " each level depth  you can change to whatever u want
                    $dat = str_repeat("- ", $level);
                    $tree[$lists->id] = $dat . $lists->name;

                    // recursive
                    $this->treeBuild($lists->children, $level);
                }
            }
        }

        return $tree;
    }

    public function scopeMain($query)
    {
        return $query->where('parent_id', null);
    }

    public function fullName($separator = ' / ')
    {
        return $this->parent->name . $separator . $this->name;
    }
}
