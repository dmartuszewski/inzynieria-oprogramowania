<?php

class Discount extends BaseModel {

	public $fillable = ['user_id', 'percent'];

	public function user()
	{
		return $this->belongsTo('User');
	}
}
