<?php

class BaseModel extends Way\Database\Model {
    protected $guarded = array();

    public static $rules = array();

    public function dateCreated()
    {
        return $this->dateToUi($this->created_at);
    }

    public function dateTimeCreated()
    {
        return $this->dateToUi($this->created_at, true);
    }

    /**
     * Converts m/d/Y to Y-m-d format
     * @param string $uiDate
     * @return string
     */
    protected function dateToDb($uiDate)
    {
        if(!$uiDate)
        {
            return null;
        }

        list($m, $d, $y) = explode('/', $uiDate);
        return $y . '-' . $m . '-' . $d;
    }

    /**
     * Converts Y-m-d to m/d/Y [H:i] format
     * @param string $uiDate
     * @param boolean $showTime Show time
     * @return string
     */
    protected function dateToUi($dbDate, $showTime = false)
    {
        if(!$dbDate)
        {
            return null;
        }

        if(strpos($dbDate, ' ') !== FALSE)
        {
            list($date, $time) = explode(' ', $dbDate);
        }
        else
        {
            $date = $dbDate;
            $time = '';
        }

        list($y, $m, $d) = explode('-', $date);

        return join('/', [$m, $d, $y]) . ($showTime ? ' ' . substr($time, 0, 5) : '');
    }
/*
    public function dateAgo()
    {

        $since = new static('now', $this->getTimezone());


        $units = array('second', 'minute', 'hour', 'day', 'week', 'month', 'year');
        $values = array(60, 60, 24, 7, 4.35, 12);

        // Date difference
        $difference = abs($since->getTimestamp() - $this->getTimestamp());

        for ($i = 0; $i < count($values) and $difference >= $values[$i]; $i++)
        {
            $difference = $difference / $values[$i];
        }

        $difference = round($difference);

        // Select unit
        $unit = $units[$i];

        // Translate
        $ago = $lang->choice("date::date.$unit", $difference, array('number' => $difference));

        // Future or past?
        if ($since->getTimestamp() < $this->getTimestamp())
        {
            return $lang->get('date::date.from now', array('time' => $ago));
        }
        else
        {
            return $lang->get('date::date.ago', array('time' => $ago));
        }
    }
*/
    public function scopeDescending($query, $field = 'id')
    {
        return $query->orderBy($field,'DESC');
    }

    public function scopeOfUser($query, $userId)
    {
        return $query->where('user_id', '=', $userId);
    }

    public function scopeOfCurrentUser($query)
    {
        return $query->where('user_id', '=', Auth::user()->id);
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    /**
     * Generates unique slug for current model
     * @param $str String to slug
     * @return string
     */
    protected function generateSlug($str)
    {
        $i = 0;
        do
        {
            $slug = Str::slug($str);

            if($i > 0)
            {
                $slug .= '-' . $i;
            }

            ++$i;
        }
        while( $this->where('slug', '=', $slug)->count() != 0);

        return $slug;
    }
}
