<?php

class Rent extends BaseModel
{

	protected $fillable = ['user_id', 'book_id', 'days'];

	public static $rules = [
		'book_id' => 'required|numeric',
		'days'    => 'required|numeric'
	];

	public function book()
	{
		return $this->belongsTo('Book');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function end()
	{
		$this->returned_at = new DateTime();

		return $this->save();
	}
}
