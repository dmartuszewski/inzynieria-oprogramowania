<?php

class Payment extends BaseModel {

	public $fillable = ['user_id', 'amount'];

	public function user()
	{
		return $this->belongsTo('User');
	}
}