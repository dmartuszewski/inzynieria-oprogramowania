<?php

class Book extends Eloquent
{
	protected $guarded = array();

	public static $rules = [
		'title' => 'required',
		'description' => 'required',
		'isbn' => 'required',
		'author' => 'required',
	];

}
