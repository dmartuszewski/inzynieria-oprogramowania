@extends('layouts.user', ['current' => 'user_rents'])

@section('content2')

@if ($rents->count())
	<div class="table-responsive">
		<table class="table table-blue mt2em">
			<thead>
			<tr>
				<th>Tytuł</th>
				<th>Author</th>
				<th>Termin oddania</th>
				<th>Oddano</th>
			</tr>
			</thead>

			<tbody>
				@foreach ($rents as $rent)
					<tr>
						<td>{{{ $rent->book->title }}}</td>
						<td>{{{ $rent->book->author }}}</td>
						<td>{{{ $rent->created_at->addDays($rent->days)->format('d/m/Y') }}}</td>
						<td class="text-center">
							@if($rent->returned_at)
							<span class="glyphicon glyphicon-ok-circle" title="Oddano"></span>
							@elseif($rent->created_at->addDays($rent->days)->lt($rent->created_at->now()))
							<span class="glyphicon glyphicon-exclamation-sign" title="Po terminie"></span>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@include('partials.pagination', ['object' => $rents])
@else
	@nothingFound('Nic jeszcze nie wypożyczyłeś')
@endif

@stop
