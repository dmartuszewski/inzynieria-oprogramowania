@extends('layouts.user', ['current' => 'rents.index'])

<?php
$items = [
    'rents.index' => 'Lista',
    'rents.create' => 'Dodaj',
    ['rents.edit', 'Edycja', [$rent->id]],
    ['rents.show', 'Podgląd', [$rent->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'rents.edit'])

@section('content2')

{{ Former::open()->route('rents.update', [$rent->id])->populate($rent) }}

<div class="form-group">
    {{ Former::select('user_id', 'Użytkownik')->options([]) }}
    {{ Former::select('book_id', 'Książka')->options([]) }}
    {{ Former::text('days')->label('Ilość dni') }}
    {{ Former::text('created_at')->label('Dodano') }}
    {{ Former::text('updated_at')->label('Ostatnia edycja') }}
</div>

{{ Former::actions()->large_primary_submit('Zapisz') }}

{{ Form::close() }}
@stop

@include('packages.select')
@include('packages.datetimepicker')
@include("packages.tmpl")