@extends('layouts.user', ['current' => 'rents.index'])

<?php
$items = [
    'rents.index' => 'Lista',
    'rents.create' => 'Dodaj',
    ['rents.edit', 'Edycja', [$rent->id]],
    ['rents.show', 'Podgląd', [$rent->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'rents.show'])

@section('content2')

    <div class="table-responsive">
        <table class="table table-blue mt2em">
            <thead>
                <tr>
                <th>ID</th>
                <th>ID użytkownika</th>
		<th>ID książki</th>
                <th>Ilość dni</th>
		<th>Utworzono</th>
		<th>Ostatnia modyfikacja</th>
		<th>Akcja</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>{{{ $rent->id }}}</td>
                    <td>{{{ $rent->user_id }}}</td>
                    <td>{{{ $rent->book_id }}}</td>
                    <td>{{{ $rent->days }}}</td>
                    <td>{{{ $rent->created_at }}}</td>
                    <td>{{{ $rent->updated_at }}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    {{ Form::open(array('class' => 'mt2em', 'method' => 'DELETE', 'route' => array('rents.destroy', $rent->id))) }}
    {{ Form::submit('Usuń alert', array('class' => 'btn btn-danger remove-submit')) }}
    {{ Form::close() }}
@stop

@include("packages.bootbox")