@extends('layouts.user', ['current' => 'user_rents'])

@section('content2')

<div class="well">
	<h2>
		{{{ $book->title }}}
		<br/>
		<small>{{{ $book->author }}}</small>
	</h2>
</div>

{{ Former::open()->route('new_rent') }}

	<div class="row">
		<div class="col-sm-7 col-sm-offset-1">
			{{ Former::hidden('book_id')->value($book->id) }}
			{{ Former::text('days', 'Na ile dni?') }}
			{{ Former::checkbox('agree', 'Akceptuję warunki i zasady')->inline() }}
		</div>
	</div>
	{{ Former::actions()->large_success_submit('Wypożycz') }}

{{ Form::close() }}

@stop

@include('packages.datetimepicker')
@include('packages.select')