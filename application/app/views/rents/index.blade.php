@extends('layouts.user', ['current' => 'rents.index'])

<?php
$items = [
	'rents.index' => 'Lista',
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'rents.index'])

@section('content2')

@if ($rents->count())
	<div class="table-responsive">
		<table class="table table-blue mt2em">
			<thead>
				<tr>
					<th>Użytkownik</th>
					<th>Książka</th>
					<th>Data</th>
					<th>Termin oddania</th>
					<th>Oddano</th>
					<th class="text-right">Akcja</th>
				</tr>
			</thead>

			<tbody>
			@foreach ($rents as $rent)
				<tr>
					<td>{{ HTML::linkRoute('users.show', $rent->user->name(), $rent->user->id) }}</td>
					<td>{{ HTML::linkRoute('books.show', $rent->book->title, $rent->book->id) }}</td>
					<td>{{{ $rent->created_at->format('d/m/Y') }}}</td>
					<td>
						{{ $rent->created_at->addDays($rent->days)->format('d/m/Y') }}
					</td>
					<td class="text-center">
						@if($rent->returned_at)
							<span class="glyphicon glyphicon-ok-circle" title="Oddano"></span>
						@elseif($rent->created_at->addDays($rent->days)->lt($rent->created_at->now()))
							<span class="glyphicon glyphicon-exclamation-sign" title="Po terminie"></span>
						@endif
					</td>

					<td class="text-right col-sm-2">
						@if(!$rent->returned_at)
							{{ Former::open()->route('end_rent') }}
							{{ Former::hidden('id', $rent->id) }}
							{{ Former::submit('Oddano')->class('btn btn-primary confirm-return') }}
							{{ Former::close() }}
						@endif
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
	@include('partials.pagination', ['object' => $rents])
@else
	@nothingFound('Nie dodano żadnych wypożyczeń!')
@endif

@stop

@include('packages.bootbox')