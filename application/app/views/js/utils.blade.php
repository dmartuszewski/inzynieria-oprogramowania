@section('js-bottom')
    @parent

    @foreach( $files as $file)
        {{ HTML::script('js/utils/' . $file . '.js') }}
    @endforeach
@stop
