<?php

if (!isset($includeType)) {
    $includeType = 'js';
}

/**
 *
 * Autoload js script for current pagepage
 *
 * Script checks if file named controller/method.js sits in public/js/pages.
 * If file was created, it's linked.
 *
 */

// UsersController@getLogin => users/login
list($controller, $action) = explode('@', str_replace('Controller', '', str_replace(array('@get', '@post', '@put'), '@', Route::currentRouteAction())));
$file = Inflector::underscore($controller) . '/' . Inflector::underscore($action);

$publicPath = $includeType . '/pages/' . $file . '.' . $includeType;
$fullPath = public_path($publicPath);

//var_dump($publicPath);

if(is_readable($fullPath))
{

    if($includeType === 'js')
    {
        echo HTML::script($publicPath);
    }
    else
    {
        echo HTML::style($publicPath);
    }

}