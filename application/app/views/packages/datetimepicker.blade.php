@section('header-css')
@parent
    {{ HTML::style('packages/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}
@stop

@section('js-bottom')
@parent
    {{ HTML::script('js/moment.min.js') }}
    {{ HTML::script('packages/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.pl.js') }}
    {{ HTML::script('packages/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}
@stop
