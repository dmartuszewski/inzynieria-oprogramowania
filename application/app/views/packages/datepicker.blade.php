@section('header-css')
@parent
{{ HTML::style('packages/datepicker/css/datepicker.css') }}
{{ HTML::style('packages/datepicker/css/datepicker3.css') }}
@stop

@section('js-bottom')
@parent
    {{ HTML::script('packages/datepicker/js/bootstrap-datepicker.js') }}
    {{ HTML::script('packages/datepicker/js/datepicker_defaults.js') }}
@stop
