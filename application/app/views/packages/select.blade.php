@section('header-css')
@parent
    {{ HTML::style('packages/select2/select2.css') }}
    {{ HTML::style('packages/select2_bootstrap/select2-bootstrap.css') }}
    {{ HTML::style('css/select2_custom.css') }}
@stop

@section('js-bottom')
@parent
    {{ HTML::script('packages/select2/select2.js') }}
@stop
