@section('header-css')
@parent
    {{ HTML::style('packages/lightbox-master/dist/ekko-lightbox.min.css') }}
@stop

@section('js-bottom')
@parent
    {{ HTML::script('packages/lightbox-master/dist/ekko-lightbox.min.js') }}
    {{ HTML::script('js/utils/lightbox.js') }}
@stop
