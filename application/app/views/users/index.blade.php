@extends('layouts.user', ['current' => 'users.index'])

<?php
$items = [
    'users.index' => 'Lista',
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'users.index'])

@section('content2')

    @if ($users->count())
        <div class="table-responsive">
            <table class="table table-blue">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Imię</th>
                        <th>Nazwisko</th>
                        {{--<th>Aktywowany</th>--}}
                        <th>Grupy</th>
                        <th class="text-right">Menu</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{{ $user->email }}}</td>
                            <td>{{{ $user->first_name }}}</td>
                            <td>{{{ $user->last_name }}}</td>
                            {{--<td>{{ $user->activated ? 'Tak' : 'Nie' }}</td>--}}
                            <td>
                                @foreach($user->getGroups() as $group)
                                    {{ $group->name }}
                                @endforeach
                            </td>
                            <td class="text-right col-sm-2">
                            {{ link_to_route('users.show', 'Podląd', array($user->id), array('class' => 'btn btn-info')) }}
                            {{ link_to_route('users.edit', 'Edycja', array($user->id), array('class' => 'btn btn-warning')) }}
                            {{ link_to_route('discounts.edit', 'Przydziel zniżkę', array($user->id), array('class' => 'btn btn-warning')) }}
                    </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['object' => $users])
    @else
        @nothingFound('Nie dodano żadnych users')
    @endif

@stop
