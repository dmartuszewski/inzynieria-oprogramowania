@extends('layouts.master')

{{-- Hide navbar --}}
@section('menu')
@stop

{{-- Hide right side of navbar --}}
@section('navbar-right-inner')
@stop

@section('content')

    <div class="row pt2em">
        <div class="col-sm-2">
            <p class="lead c-orange">Dobra robota!</p>
        </div>

        <div class="col-sm-10">
            To zajmie tylko chwilę, ale pozwoli Ci dotrzeć do wielu odbiorców!
        </div>
    </div>

    @include('partials.registration_steps', ['step' => 1])

@stop

@section('content-full-width')
    <hr class="large"/>
@stop

@section('content2')
    {{ Former::open() }}

  

    <p class="c-dark-brown lead">
        Szczegóły konta
    </p>

    <div class="row">
        <div class="col-sm-7 col-sm-offset-1">
            {{ Former::text('first_name')->label('Imię') }}
            {{ Former::text('last_name')->label('Nazwisko') }}
            {{ Former::text('email')->label('E-mail') }}
            {{ Former::password('password')->label('Hasło') }}
            {{ Former::password('password_confirmation')->label('Potwierdź hasło') }}
        </div>
    </div>

    <div class="clearfix"></div>

	{{ Former::actions( Button::submit_warning('Załóż konto')) }}


    {{ Former::close() }}

@stop
