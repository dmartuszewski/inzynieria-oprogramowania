@extends('layouts.master')

@section('body_class') login @stop

{{-- Hide navbar --}}
@section('menu') @stop

@section('header-right-menu') @stop

@section('content-full-width')
    <div class="login-form-wrapper ">

        <div class="pt3em pb2em">

            <div class="container">
                <h1 class="text-center pb1em">Zaloguj się, aby kontynuować</h1>
                <div class="row form-outline-div">
                    <div class="col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2 well well-lg well-bd">

                        <div class="row">

                            <div class="col-sm-4 login-part">
                                <p class="lead">
                                    Masz już konto?
                                    @if(empty($error))
                                        <br class="visible-md visible-lg"/>
                                        <br class="visible-md visible-lg"/>
                                    @endif
                                </p>
                                @if( !empty($error))
                                    <p class="alert alert-danger">
                                        {{ $error }}
                                    </p>
                                @endif

                                {{ Former::open_vertical('login') }}
                                {{ Former::text('email')->label(null)->placeholder('E-mail') }}
                                {{ Former::password('password')->label(null)->placeholder('Hasło') }}
                                <p class="text-center">
                                    {{ Button::submit_info('Zaloguj się', ['class' => 'btn-large']) }}
                                </p>
                                <p class="text-center">
                                    {{ HTML::linkRoute('password_restore_request', 'Nie pamiętam hasła.') }}
                                </p>
                                {{ Former::close() }}
                            </div>
                            <div class="col-sm-8 register-part">
                                <p class="lead">Konto jest darmowe i pozbawione jakichkolwiek ukrytych opłat!</p>

                                <p class="pt2em text-center">
                                    {{ HTML::linkRoute('register', 'Utwórz konto', [], ['class' => 'btn btn-large btn-warning lead']) }}
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

@stop
