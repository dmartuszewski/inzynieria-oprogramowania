@extends('layouts.user', ['current' => 'users.index'])

<?php
$items = [
    'users.index' => 'Lista',
    ['users.edit', 'Edycja', [$user->id]],
    ['users.show', 'Podgląd', [$user->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'users.show'])

@section('content2')

    <div class="table-responsive">
        <table class="table table-blue">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Imię</th>
                    <th>Nazwisko</th>
                    <th>Grupy</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>{{{ $user->email }}}</td>
					<td>{{{ $user->first_name }}}</td>
					<td>{{{ $user->last_name }}}</td>
                    <td>
                        @foreach($user->getGroups() as $group)
                        {{ $group->name }}
                        @endforeach
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    {{-- Form::open(array('class' => 'mt2em', 'method' => 'DELETE', 'route' => array('users.destroy', $user->id))) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-danger remove-submit')) }}
    {{ Form::close() --}}
@stop

@include("packages.bootbox")
