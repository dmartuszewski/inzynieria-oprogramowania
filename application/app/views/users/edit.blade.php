@extends('layouts.user', ['current' => 'users.index'])

<?php
$items = [
    'users.index' => 'Lista',
    ['users.edit', 'Edycja', [$user->id]],
    ['users.show', 'Podgląd', [$user->id]]
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'users.edit'])

@section('content2')

    {{ Former::open()->route('users.update', $user->id)->populate($user) }}

    {{ Former::text('first_name', 'Imię')->placeholder('Imię') }}
    {{ Former::text('last_name', 'Nazwisko')->placeholder('Nazwisko') }}

    {{ Former::select('groups[]', 'Grupa')->options($groups)->select($userGroups)->id('groups') }}
    {{ Former::password('password', 'Hasło')->placeholder('Pozostaw puste, aby nie zmieniać') }}

    {{ Former::actions()->large_primary_submit('Zapisz') }}

    {{ Former::close() }}

@stop

@include('packages.select')