@extends('layouts.master')

@section('body_class') password-reset @stop

{{-- Hide navbar --}}
@section('menu') @stop

@section('header-right-menu') @stop

@section('content-full-width')
<div class="login-form-wrapper bg-clouds">

    <div class="pt3em pb2em">

        <div class="container">
            <h1 class="text-center pb1em">Nie pamiętasz hasła?</h1>
            <div class="row form-outline-div">
                <div class="col-sm-4 col-sm-offset-4 well well-lg well-bd pt3em">

                    <div  class="login-part">
                        <p class="pb2em">
                            Proszę wpisz swój e-mail, który podałeś podczas rejestracji.
                        </p>

                        @if( !empty($error))
                            <p class="alert alert-danger">
                                {{ $error }}
                            </p>
                        @endif

                        {{ Former::open_vertical() }}
                        {{ Former::text('email')->label(null)->placeholder('Email')->required() }}
                        <p class="text-center">
                            {{ Button::submit_warning('Wyślij wiadomość e-mail', ['class' => 'btn-large']) }}
                        </p>
                        <p class="text-center @if(empty($error)) pt3em @endif">
                            {{ HTML::linkRoute('login', 'Powrót do logowania') }}
                        </p>
                        {{ Former::close() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@stop

@section('header-css')
    {{ HTML::style('css/pages/users/login.css') }}
@stop

@section('js-bottom')
{{ HTML::script('js/pages/users/login.js') }}
@stop
