@extends('emails.layouts.master')

@section('header')
    <h2>Witaj {{ $first_name }}, </h2>
    <h3>Dziękujemy za rejestrację w serwisie Localeo</h3>
@stop

@section('content')
    W celu dokończenia rejestracji kliknij link: {{ URL::route('activate_account', [$activation_code]) }}.
@stop