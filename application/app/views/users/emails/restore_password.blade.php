@extends('emails.layouts.master')

@section('header')
    <h2>Hi {{ $first_name }}, </h2>
    <h3>You say that you forget your password?</h3>
@stop

@section('content')
    <p>To change password, please click the following link: {{ URL::route('reset_password', ['code' => $reset_password_code]) }}.</p>

    <p>If you haven't requested password reset ignore this email.</p>
@stop