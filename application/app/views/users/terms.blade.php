@extends('layouts.master');

{{-- Hide navbar --}}
@section('menu')
@stop

{{-- Hide right side of navbar --}}
@section('navbar-right-inner')
@stop

@section('content')

    <div class="row pt2em">
        <div class="col-sm-2">
            <p class="lead c-orange">Dobra robota!</p>
        </div>

        <div class="col-sm-10">
            Jeszcze moment, przeczytaj regulamin!
        </div>
    </div>

    @include('partials.registration_steps', ['step' => 2])

@stop

@section('content-full-width')
    <hr class="large"/>
@stop

@section('content2')

    <h1>Regulamin</h1>
    <p class="lead">Przeczytaj i zaakceptuj</p>

    {{ Former::open() }}

    <div class="well">
            Zasady:
        <ul>
        <li>na konta można dodawać tylko własne zdjęcia</li>
        <li>zdjęcie może posiadać minimalną graficzną modyfikację (np. ramka, krótki napis)
        dozwolony jest collage utworzony tylko z własnych zdjęć</li>
        <li>nie ma opłat ani limitów, byle nie wrzucać 5 tys. zdjęć dziennie</li>
        <li>użytkownik może posiadać jedno konto w serwisie</li>
        </ul>

    Niedozwolone zdjęcia i treści:

       <ul> przedstawiające:
           <li>rozmyślnie zakrytą nagość;</li>
            <li>treści pornograficzne;</li>
            <li>przezroczyste ubrania;</li>
            <li>lubieżne lub prowokacyjne pozy;</li>
            <li>zbliżenia piersi, pośladków lub krocza;</li>
        <li>propagujące przemoc, nawołujące do nienawiści, obraźliwe</li>
        <li>naruszające prawo do prywatności, prawo autorskie</li>
        <li>obrazy stworzone przy pomocy programów graficznych</li>
        <li>komentarze i wiadomości nie mogą zawierać treści wulgarnych, obraźliwych, niezgodnych z prawem oraz ogólnie przyjętymi normami społecznymi</li>
        </ul>
    Inne rzeczy:

        niedozwolone zdjęcia mogą być usuwane bez ostrzeżenia
        konta mogą być blokowane bez ostrzeżenia
        nie ma gwarancji co do niezawodności działania strony
        regulamin może ulegać zmianom

    </div>

    {{ Former::checkbox('terms_accepted_at')->label('')->text("Zapoznałem się z treścią regulaminu.") }}

    <div class="text-right">
        {{ Former::actions( Button::submit_warning('Dalej')) }}
    </div>

    {{ Former::close() }}

@stop
