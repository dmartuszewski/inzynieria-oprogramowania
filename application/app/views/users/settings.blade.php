@extends('layouts.user', ['current' => 'settings'])

@section('content')
    <h2>Ustawienia konta</h2>
@stop

@section('content2')

    {{ Former::open()->populate($user) }}

    {{ Former::text('first_name', 'Imię')->placeholder('Imię') }}
    {{ Former::text('last_name', 'Nazwisko')->placeholder('Nazwisko') }}
    {{ Former::password('password', 'Hasło')->placeholder('Pozostaw puste, aby nie zmieniać') }}
    {{ Former::actions()->large_primary_submit('Zapisz') }}

    {{ Former::close() }}

@stop