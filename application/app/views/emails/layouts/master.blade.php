<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
<body>
    <div>
        @yield('header')
    </div>

    <div>
        @yield('content')
    </div>

</body>
</html>