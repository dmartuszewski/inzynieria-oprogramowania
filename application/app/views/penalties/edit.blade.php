@extends('layouts.user', ['current' => 'alerts.index'])

<?php
$items = [
    'penalties.index' => 'Lista',
    'penalties.create' => 'Dodaj',
    ['penalties.edit', 'Edycja', [$penalty->id]],
    ['penalties.show', 'Podgląd', [$penalty->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'penalties.edit'])


@section('content2')

	{{ Former::open()->route('penalties.update', [$penalty->id])->populate($penalty) }}

	@include('forms.penalty')

@stop

@include('packages.select')