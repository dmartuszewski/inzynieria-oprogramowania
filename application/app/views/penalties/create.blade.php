@extends('layouts.user', ['current' => 'penalties.index'])

<?php
$items = [
	'penalties.index' => 'Lista',
	'penalties.create' => 'Dodaj',
];
?>

@include('partials.mid_menu_list', ['items' => $items, 'current' => 'penalties.create'])

@section('content2')

     {{ Former::open()->route('penalties.store') }}

	@include('forms.penalty')

@stop

@include('packages.select')
