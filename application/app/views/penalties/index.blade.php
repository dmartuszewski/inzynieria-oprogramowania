@extends('layouts.user', ['current' => 'penalties.index'])

<?php
$items = [
    'penalties.index' => 'Lista',
    'penalties.create' => 'Dodaj'
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'penalties.index'])

@section('content2')

    @if ($penalties->count())
        <div class="table-responsive">
            <table class="table table-blue mt2em">
                <thead>
                <tr>
                <th>Użytkownik</th>
		<th>Wartość kary</th>
		<th>Utworzono</th>
		<th>Ostatnia modyfikacja</th>
		<th>Akcja</th>
                </tr>
            </thead>

                <tbody>
                    @foreach ($penalties as $penalty)
                        <tr>
                            <td>{{{ $penalty->user->name() }}}</td>
                            <td>{{{ $penalty->amount }}} PLN</td>
                            <td>{{{ $penalty->created_at->addDays($penalty->days)->format('d/m/Y') }}}</td>
                            <td>{{{ $penalty->updated_at->addDays($penalty->days)->format('d/m/Y') }}}</td>
                            <td class="text-right  col-sm-2">
                                {{ link_to_route('penalties.show', 'Podląd', array($penalty->id), array('class' => 'btn btn-info')) }}
                                {{ link_to_route('penalties.edit', 'Edycja', array($penalty->id), array('class' => 'btn btn-warning')) }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['object' => $penalties])
    @else
        @nothingFound('Nie ma żadnych kar!')
    @endif

@stop
