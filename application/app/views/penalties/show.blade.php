@extends('layouts.user', ['current' => 'penalties.index'])

<?php
$items = [
    'penalties.index' => 'Lista',
    'penalties.create' => 'Dodaj',
    ['penalties.edit', 'Edycja', [$penalty->id]],
    ['penalties.show', 'Podgląd', [$penalty->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'penalties.show'])

@section('content2')

    <div class="table-responsive">
        <table class="table table-blue mt2em">
            <thead>
                <tr>
		<th>Wartość kary</th>
		<th>Utworzono</th>
		<th>Ostatnia modyfikacja</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>{{{ $penalty->amount }}} PLN</td>
            	    <td>{{{ $penalty->created_at->addDays($penalty->days)->format('d/m/Y') }}}</td>
                    <td>{{{ $penalty->updated_at->addDays($penalty->days)->format('d/m/Y') }}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    {{ Form::open(array('class' => 'mt2em', 'method' => 'DELETE', 'route' => array('penalties.destroy', $penalty->id))) }}
    {{ Form::submit('Usuń karę', array('class' => 'btn btn-danger remove-submit')) }}
    {{ Form::close() }}
@stop

@include("packages.bootbox")