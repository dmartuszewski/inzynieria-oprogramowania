@extends('layouts.master')

@section('body_class') home @stop

{{-- Hide navbar in the place where it is placed orginary - will be showed later in overrided header_extend section --}}
@section('menu')
@stop

{{-- Hide right side of navbar --}}
@section('navbar-right-inner')
@stop

@section('header-extend')

    @include('partials.navbar')

    <div class="row">
        <div class="col-sm-7 col-xs-7 font-xxl">
            Wystarczy parę kliknieć<br />
	    aby wypożyczyć książkę<br />
	    o której marzysz!
        </div>
      <!--  <div class="col-sm-5 col-xs-5  main-actions-btns">
            <a class="btn btn-xl btn-xl-yellow" href="#">
                <span>Need skilled people?</span>
                Post a Project
            </a>

            <a class="btn btn-xl btn-xl-blue" href="#">
                <span>Looking for clients?</span>
                Provide Services
            </a>
        </div>-->
    </div>

    <p class="pt3em pb1em main-description">

    </p>
@stop

@section('content')

@stop

@section('content2')

    <div class="row pt3em">
        <div class="col-sm-4">
            {{ HTML::image('images/home_page_social_graph.png', 'IO social graph', ['class' => 'img-responsive'])}}
        </div>
        <div class="col-sm-8">
            <p class="lead">Kochamy czytać!</p>

            <p>Wejdź w świat książek i pokochaj czytanie z projektem z IO.
            </p>

            <p class="pb1em">

            </p>

        </div>
    </div>

@stop
