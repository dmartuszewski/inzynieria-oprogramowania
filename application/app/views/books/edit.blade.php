@extends('layouts.user', ['current' => 'books.index'])

<?php
$items = [
	'books.index' => 'Lista',
	'books.create' => 'Dodaj książkę',
	['books.edit', 'Edycja', [$book->id]],
	['books.show', 'Podgląd', [$book->id]],
];
?>

@include('partials.mid_menu_list', ['items' => $items, 'current' => 'books.edit'])

@section('content2')

	{{ Former::open()->route('books.update', $book->id)->populate($book) }}

	@include('forms.book')

@stop

