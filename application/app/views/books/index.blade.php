@extends('layouts.user', ['current' => 'books.index'])

<?php
$items = [
    'books.index' => 'Lista',
    'books.create' => 'Dodaj książkę'
];
?>

@include('partials.mid_menu_list', ['items' => $items, 'current' => 'books.index'])

@section('content2')

    @if ($books->count())
        <div class="table-responsive">
            <table class="table table-blue">
                <thead>
                    <tr>
                        <th class="col-sm-3">Tytuł</th>
                        <th class="col-sm-2">ISBN</th>
                        <th>Autor</th>
                        <th>Rok</th>
                        <th class="text-right col-sm-3">Menu</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($books as $book)
                        <tr>
                            <td>{{{ $book->title }}}</td>
							<td>{{{ $book->isbn }}}</td>
							<td>{{{ $book->author }}}</td>
							<td>{{{ $book->year }}}</td>
							<td class="text-right  col-sm-4">
								{{ link_to_route('books.show', 'Podgląd', array($book->id), array('class' => 'btn btn-info')) }}
								{{ link_to_route('books.edit', 'Edycja', array($book->id), array('class' => 'btn btn-warning')) }}

								{{ Former::open()->class('form-inline')->route('books.destroy', $book->id) }}
								{{ Form::submit('Usuń', array('class' => 'btn btn-danger remove-submit')) }}
								{{ Form::close() }}
							</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['object' => $books])
    @else
        @nothingFound('Nie dodano żadnych książek')
    @endif

@stop

@include('packages.bootbox')