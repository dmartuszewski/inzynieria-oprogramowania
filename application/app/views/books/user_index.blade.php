@extends('layouts.user', ['current' => $current])

@section('content2')

	@if($current == 'books_search')
		{{ Former::open()->method('get') }}
		{{ Former::text('title', 'Tytuł') }}
		{{ Former::text('description', 'Opis') }}
		{{ Former::actions()->large_primary_submit('Wyślij') }}
		{{ Former::close() }}
	@endif

    @if ($books->count())
        <div class="table-responsive">
            <table class="table table-blue">
                <thead>
                    <tr>
                        <th class="col-sm-3">Tytuł</th>
                        <th class="col-sm-2">Autor</th>
                        <th>Opis</th>
                        <th>Dodano</th>
                        <th class="text-right">Menu</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($books as $book)
                        <tr>
                            <td>{{{ $book->title }}}</td>
							<td>{{{ $book->author }}}</td>
							<td>{{{ $book->description }}}</td>
							<td>{{{ $book->created_at->format('d/m/Y') }}}</td>
							<td class="text-right col-sm-3">
								{{ HTML::linkRoute('new_rent', 'Wypożycz', $book->id, ['class' => 'btn btn-success']) }}
							</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['object' => $books])
    @else
        @nothingFound('Brak książek do wyświetlenia')
    @endif

@stop
