@extends('layouts.user', ['current' => 'books.index'])

<?php
$items = [
    'books.index' => 'Lista',
    'books.create' => 'Dodaj książkę',
    ['books.edit', 'Edycja', [$book->id]],
    ['books.show', 'Podgląd', [$book->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'books.show'])

@section('content2')

	<dl class="ul-horizontal">
		<dt>Tytuł</dt> <td>{{{ $book->title }}}</td>
		<dt>Opis</dt> <td>{{{ $book->description }}}</td>
		<dt>Isbn</dt> <td>{{{ $book->isbn }}}</td>
		<dt>Autor</dt> <td>{{{ $book->author }}}</td>
		<dt>Rok</dt> <td>{{{ $book->year }}}</td>

	</dl>

    {{ Form::open(array('class' => 'mt2em mb2em', 'method' => 'DELETE', 'route' => array('books.destroy', $book->id))) }}
    {{ Form::submit('Usuń książkę', array('class' => 'btn btn-danger remove-submit')) }}
    {{ Form::close() }}
@stop

@include("packages.bootbox")
