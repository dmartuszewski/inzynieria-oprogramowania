@extends('layouts.user', ['current' => 'books.index'])

<?php
$items = [
	'books.index' => 'Lista',
	'books.create' => 'Dodaj książkę',
];
?>

@include('partials.mid_menu_list', ['items' => $items, 'current' => 'books.create'])

@section('content2')

	{{ Former::open()->route('books.store') }}

	@include('forms.book')

@stop

