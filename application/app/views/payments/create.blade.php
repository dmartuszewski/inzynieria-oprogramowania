@extends('layouts.user', ['current' => 'payments.index'])

<?php
$items = [
	'payments.index' => 'Lista',
	'payments.create' => 'Dodaj',
];
?>

@include('partials.mid_menu_list', ['items' => $items, 'current' => 'payments.create'])

@section('content2')

    {{ Former::open()->route('payments.store') }}

	@include('forms.payment')

@stop

@include('packages.select')
