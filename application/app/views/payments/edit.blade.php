@extends('layouts.user', ['current' => 'payments.index'])

<?php
$items = [
    'payments.index' => 'Lista',
    'payments.create' => 'Dodaj',
    ['payments.edit', 'Edycja', [$payment->id]],
    ['payments.show', 'Podgląd', [$payment->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'payments.edit'])

@section('content2')

	{{ Former::open()->route('payments.update', [$payment->id])->populate($payment) }}

	@include('forms.payment')

@stop

@include('packages.select')