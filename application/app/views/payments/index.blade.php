@extends('layouts.user', ['current' => 'payments.index'])

<?php
$items = [
    'payments.index' => 'Lista',
    'payments.create' => 'Dodaj'
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'payments.index'])

@section('content2')

    @if ($payments->count())
        <div class="table-responsive">
            <table class="table table-blue mt2em">
                <thead>
                <tr>
                <th>Użytkownik</th>
		<th>Wartość płatności</th>
		<th>Utworzono</th>
		<th>Ostatnia modyfikacja</th>
		<th>Akcja</th>
                </tr>
            </thead>

                <tbody>
                    @foreach ($payments as $payment)
                        <tr>
                            <td>{{{ $payment->user->name() }}}</td>
                            <td>{{{ $payment->amount }}} PLN</td>
                            <td>{{{ $payment->created_at->addDays($payment->days)->format('d/m/Y') }}}</td>
                            <td>{{{ $payment->updated_at->addDays($payment->days)->format('d/m/Y') }}}</td>
                            <td class="text-right  col-sm-2">
                                {{ link_to_route('payments.show', 'Podląd', array($payment->id), array('class' => 'btn btn-info')) }}
                                {{ link_to_route('payments.edit', 'Edycja', array($payment->id), array('class' => 'btn btn-warning')) }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['object' => $payments])
    @else
        @nothingFound('Nie ma żadnych płatności!')
    @endif

@stop
