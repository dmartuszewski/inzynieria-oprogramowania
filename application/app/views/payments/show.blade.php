@extends('layouts.user', ['current' => 'payments.index'])

<?php
$items = [
    'payments.index' => 'Lista',
    'payments.create' => 'Dodaj',
    ['payments.edit', 'Edycja', [$payment->id]],
    ['payments.show', 'Podgląd', [$payment->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'payments.show'])

@section('content2')

    <div class="table-responsive">
        <table class="table table-blue mt2em">
            <thead>
                <tr>
                <th>Użytkownik</th>
		<th>Wartość wpłaty</th>
		<th>Utworzono</th>
		<th>Ostatnia modyfikacja</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>{{{ $payment->user->name() }}}</td>
                    <td>{{{ $payment->amount }}} PLN</td>
            	    <td>{{{ $payment->created_at->addDays($payment->days)->format('d/m/Y') }}}</td>
                    <td>{{{ $payment->updated_at->addDays($payment->days)->format('d/m/Y') }}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    {{ Form::open(array('class' => 'mt2em', 'method' => 'DELETE', 'route' => array('payments.destroy', $payment->id))) }}
    {{ Form::submit('Usuń płatność', array('class' => 'btn btn-danger remove-submit')) }}
    {{ Form::close() }}
@stop

@include("packages.bootbox")