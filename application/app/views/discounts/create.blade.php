@extends('layouts.user', ['current' => 'discounts.index'])

<?php
$items = [
	'discounts.index' => 'Lista',
	'discounts.create' => 'Dodaj',
];
?>

@include('partials.mid_menu_list', ['items' => $items, 'current' => 'discounts.create'])

@section('content2')

    {{ Former::open()->route('discounts.store') }}

	@include('forms.discount')

@stop

@include('packages.select')
