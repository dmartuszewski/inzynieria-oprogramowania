@extends('layouts.user', ['current' => 'discounts.index'])

<?php
$items = [
    'discounts.index' => 'Lista',
    'discounts.create' => 'Dodaj'
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'discounts.index'])

@section('content2')

    @if ($discounts->count())
        <div class="table-responsive">
            <table class="table table-blue mt2em">
                <thead>
					<tr>
						<th>Użytkownik</th>
						<th>Wartość zniżki</th>
						<th>Utworzono</th>
						<th>Ostatnia modyfikacja</th>
						<th>Akcja</th>
					</tr>
				</thead>

                <tbody>
                    @foreach ($discounts as $discount)
                        <tr>
                            <td>{{{ $discount->user->name() }}}</td>
                            <td>{{{ $discount->percent }}}%</td>
                            <td>{{{ $discount->created_at->addDays($discount->days)->format('d/m/Y') }}}</td>
                            <td>{{{ $discount->updated_at->addDays($discount->days)->format('d/m/Y') }}}</td>
                            <td class="text-right  col-sm-2">
                                {{ link_to_route('discounts.show', 'Podląd', array($discount->id), array('class' => 'btn btn-info')) }}
                                {{ link_to_route('discounts.edit', 'Edycja', array($discount->id), array('class' => 'btn btn-warning')) }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['object' => $discounts])
    @else
        @nothingFound('Nie dodano żadnych zniżek!')
    @endif

@stop
