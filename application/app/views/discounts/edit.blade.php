@extends('layouts.user', ['current' => 'discounts.index'])

<?php
$items = [
    'discounts.index' => 'Lista',
    'discounts.create' => 'Dodaj',
    ['discounts.edit', 'Edycja', [$discount->id]],
    ['discounts.show', 'Podgląd', [$discount->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'discounts.edit'])

@section('content2')

	{{ Former::open()->route('discounts.update', [$discount->id])->populate($discount) }}

	@include('forms.discount')

@stop

@include('packages.select')

