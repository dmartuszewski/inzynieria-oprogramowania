@extends('layouts.user', ['current' => 'discounts.index'])

<?php
$items = [
    'discounts.index' => 'Lista',
    'discounts.create' => 'Dodaj',
    ['discounts.edit', 'Edycja', [$discount->id]],
    ['discounts.show', 'Podgląd', [$discount->id]],
];
?>
@include('partials.mid_menu_list', ['items' => $items, 'current' => 'discounts.show'])

@section('content2')

    <div class="table-responsive">
        <table class="table table-blue mt2em">
            <thead>
                <tr>
					<th>Użytkownik</th>
					<th>Procent</th>
					<th>Utworzono</th>
					<th>Ostatnia modyfikacja</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>{{{ $discount->user->name() }}}</td>
                    <td>{{{ $discount->percent }}}%</td>
            	    <td>{{{ $discount->created_at->addDays($discount->days)->format('d/m/Y') }}}</td>
                    <td>{{{ $discount->updated_at->addDays($discount->days)->format('d/m/Y') }}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    {{ Form::open(array('class' => 'mt2em', 'method' => 'DELETE', 'route' => array('discounts.destroy', $discount->id))) }}
    {{ Form::submit('Usuń zniżkę', array('class' => 'btn btn-danger remove-submit')) }}
    {{ Form::close() }}
@stop

@include("packages.bootbox")