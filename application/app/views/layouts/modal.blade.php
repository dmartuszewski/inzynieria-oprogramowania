<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    @section('title-wrapper')
        <h4 class="modal-title">
            @yield('title')
        </h4>
    @show
</div>

@section('body-wrapper')
    <div class="modal-body">
        @yield('body')
    </div>
@stop

@section('footer-wrapper')
    <div class="modal-footer">
        @section('footer')
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        @show
    </div>
@show

@section('js-bottom')
@show

@include('js.page_script')