@section('head')
    <head>
        <meta charset="utf-8">
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css'); }}
        {{ HTML::style('css/main.css'); }}

        @section('header-css')
        @show

        @include('js.page_script', ['includeType' => 'css'])

        {{ HTML::script('js/jquery.js'); }}
        {{ HTML::script('packages/bootstrap/js/bootstrap.min.js'); }}
    </head>
@show
