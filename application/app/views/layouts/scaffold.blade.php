<!DOCTYPE html>
<html lang="en-US">

    @include('layouts.head')
    {{ HTML::style('css/layouts/user.css'); }}

<body class="user-section @yield('body_class')">

    @include('partials.header')

    @include('partials.navbar_user')

    <div class="container">

        @section('flash-wrap')
            @include('partials.flash')
        @show

        @yield('content')

    </div>

    @section('mid-menu')
        <div id="mid-menu">
            @yield('mid-menu-list')
        </div>
    @show

    <div class="container pt2em">
        @yield('content2')
    </div>

    @section('footer-posts') @stop
    @section('footer_class') light @stop
    @include('partials.footer')

    {{ HTML::script('js/layouts/user.js') }}
</body>
</html>