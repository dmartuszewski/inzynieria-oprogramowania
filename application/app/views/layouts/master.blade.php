<!DOCTYPE html>
<html lang="en-US">

@include('layouts.head')

<body class="@yield('body_class')">

    @include('partials.header')

    @section('menu')
        @include('partials.navbar')
    @stop

    <div class="container">

        @section('flash-wrap')
            @include('partials.flash')
        @show

        @yield('content')

    </div>

    @yield('content-full-width')

    <div class="container">
        @yield('content2')
    </div>

    @include('partials.footer')

</body>
</html>