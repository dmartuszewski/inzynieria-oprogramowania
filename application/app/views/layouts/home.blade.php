<!DOCTYPE html>
<html lang="en-US">
    @include('layouts.head')
<body class="home">

    @include('partials.header')

    @include('partials.flash')

    <div class="container">

        <div class="row-fluid">
            <div class="col-sm-9">

                @yield('content')

            </div>

            <div class="col-sm-3">
                @section('sidebar')

                @show
            </div>
        </div>

    </div>

    @include('partials.footer')

</body>
</html>