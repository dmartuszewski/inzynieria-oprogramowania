@section('header')
    <header class="@yield('header_class')">
        <div class="container">

            <div class="row">
                <h1 class="col-sm-6 col-xs-5">
                    <a href="/">
                        {{ HTML::image('images/logowhite.png', 'Localeo logo', ['class' => 'img-responsive'])}}
                    </a>
                </h1>

                <div class="col-xs-1 navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                @section('header-right-menu')
                    <div class="col-sm-6 col-xs-6 text-right pt1em">

                        @if( Auth::check() )

                            Witaj, <b>{{ HTML::linkRoute('settings', Auth::user()->first_name) }}</b>
                            {{ HTML::linkRoute('logout', 'Wyloguj') }}

                        @else

                            {{ Html::link('login', 'Logowanie') }}
                            {{ Html::linkRoute('register', 'Rejestracja', [], ['class' => 'btn-blue']) }}

                        @endif

                    </div>
                @show
            </div>

            @yield('header-extend')

        </div>
    </header>
@show
