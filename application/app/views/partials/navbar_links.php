<?php
// Collect menu items
$items = [];

if(Auth::check())
{
	$itemsUser = [
		'Moje wypożyczenia' => 'user_rents',
		'Szukaj' => 'books_search',
		'Najnowsze' => 'latest_books'
	];

	$itemsAdmin = [
		'Książki' => 'books.index',
		'Użytkownicy' => 'users.index',
		'Wypożyczenia' => 'rents.index',
		'Zniżki' => 'discounts.index',
		'Płatności' => 'payments.index',
		'Kary' => 'penalties.index',
	];

	// User
	if(Auth::user()->inGroup(Sentry::findGroupById(2)))
	{
		$items += $itemsUser;
	}

	$items['Konto'] = 'settings';

	if(Auth::user()->inGroup(Sentry::findGroupById(1)))
	{
		$items = $itemsAdmin;
	}

	if(!isset($current)) { $current = false; }
}