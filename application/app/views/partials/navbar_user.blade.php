<?php include(Config::get("view.paths.0")."/partials/navbar_links.php"); ?>

@section('navbar')
    <nav class="navbar" role="navigation">
        <div class="container">

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse-1">

                <ul class="nav navbar-nav">
                    @foreach($items as $label => $route)

                        <li>
                            {{ HTML::linkRoute($route, $label, [], ['class' => ($current == $route ? 'active' : '')]) }}
                        </li>

                    @endforeach
                </ul>


            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
@show