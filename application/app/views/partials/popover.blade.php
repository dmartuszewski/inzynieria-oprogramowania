<?php
    if(!isset($popoverTitle))
    {
        $popoverTitle = false;
    }
?>
<a href="#"
   class="btn btn-info"
   data-toggle="popover" title=""
   data-content="{{{ $popoverContent }}}"
   role="button"
   @if($popoverTitle) data-original-title="{{{ $popoverTitle }}}" @endif>
    <span class="glyphicon glyphicon-search"></span>
</a>