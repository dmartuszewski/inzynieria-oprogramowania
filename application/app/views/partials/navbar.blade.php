<?php include(Config::get("view.paths.0")."/partials/navbar_links.php"); ?>
@section('navbar')
    <nav class="navbar" role="navigation">
        <div class="container">

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse row" id="navbar-collapse-1">

                <ul class="nav navbar-nav">
                    @if(Auth::check())
                        @foreach($items as $label => $route)
                            <li>
                                {{ HTML::linkRoute($route, $label) }}
                            </li>
                        @endforeach
                    @endif
                </ul>

                <div class="col-sm-2 text-right pt1em">
                    @section('navbar-right-inner')
                        {{ Icon::envelope() }} <a href="">Zaloguj sie, aby zobaczyc wiadomosci</a>
                    @show
                </ul>

            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
@show
