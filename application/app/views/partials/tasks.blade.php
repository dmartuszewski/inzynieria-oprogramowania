<div id="tasks">
    {{ Former::open()->route('tasks.store')->class('alert nmb')->id('task-form') }}
    {{ Former::hidden('project_id', $project->id) }}
    <div class="row pb1em">
        <div class="col-sm-10">
            {{ Former::text('task', false)->placeholder('type new task here')->required()->autocomplete('off')->raw() }}
        </div>
        <div class="col-sm-2">
            {{ Former::submit('Add')->class('btn btn-primary submit') }}
        </div>
    </div>
    {{ Form::close() }}
    <ul class="list-unstyled pl1em" id="tasks-list">

        @foreach($project->tasks as $task)
            <li class="font-s">
                <a href="#"
                    data-slug="{{ $task->slug }}"
                    class="check-task @if($task->complete()) task-complete @endif nd"
                    title="Mark as @if($task->complete()) Incomplete @else Complete @endif">

                    @if($task->complete())
                        {{ Icon::check() }}
                    @else
                        {{ Icon::unchecked() }}
                    @endif
                </a>
                {{ $task->task }}

                <span class="text-muted">
                    @if($task->complete())
                        Complete by @if($task->user_complete->id == Auth::user()->id) you @else {{ $task->user_complete->name() }} @endif on {{ $task->complete_at }}
                    @endif
                </span>
            </li>
        @endforeach

        @if(!$project->tasks->count())
            {{ Alert::info('No active tasks found') }}
        @endif
    </ul>
</div>

<script type="text/x-jquery-tmpl" id="template-new-task">
    <li class="new-task h font-s">
        <a href="#"
            data-slug="${ slug }"
            class="check-task nd"
            title="Mark as Complete">

            {{ Icon::unchecked() }}
        </a>
        ${ task }
    </li>
</script>

@include("js.utils", ['files' => ['tasks']])
@include("packages.tmpl")