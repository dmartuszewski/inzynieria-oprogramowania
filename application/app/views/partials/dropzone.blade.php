@section('header-css')
@parent
{{ HTML::style('packages/dropzone/css/dropzone.css') }}
{{ HTML::style('packages/dropzone/css/custom.css') }}
@stop

@section('js-bottom')
@parent
    {{ HTML::script('packages/dropzone/dropzone.min.js') }}
@stop
