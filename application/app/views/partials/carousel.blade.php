<?php
    $nFiles = count($files);
?>
<div id="files-carousel" class="carousel slide" data-ride="carousel" style="width:900px">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#files-carousel" data-slide-to="0" class="active"></li>
        @for($i = 1; $i < $nFiles; ++$i)
            <li data-target="#files-carousel" data-slide-to="{{ $i }}"></li>
        @endfor
    </ol>

    <?php $i = 0; ?>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        @foreach($files as $file)
            <div class="item @if($i++ === 0) active @endif">
                <img src="{{ $file->url('slide') }}" alt="Zdjęcie budynku">
            </div>
        @endforeach
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#files-carousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#files-carousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>