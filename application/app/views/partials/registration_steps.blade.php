<div class="row pt1em text-center c-dark-brown">
    <div class="col-sm-2 col-sm-offset-1 lead pt1em @if($step == 1) well @endif">
        1 - podstawowe informacje
    </div>

    <div class="col-sm-2 pt2em">
        <hr/>
    </div>

    <div class="col-sm-2 col-sm lead pt1em @if($step == 2) well @endif">
        2 - zasady korzystania z serwisu
    </div>


    <div class="col-sm-2 pt2em">
        <hr/>
    </div>

    <div class="col-sm-2 lead pt1em @if($step == 3) well @endif">
        3 - podsumowanie
    </div>
</div>
