<dl class="dl-horizontal dl-dt-left pl1em">
    <dt>Project</dt>
    <dd>{{ HTML::linkRoute('project_preview', $proposal->project->name, [$proposal->project->slug]) }}</dd>
    <dt>Pay Type</dt>
    <dd>
        @if( $proposal->negotiation->accepted_by )
            {{ $proposal->negotiation->payType->name }}
        @else
            {{ $proposal->payType->name }}
        @endif
    </dd>
    <dt>Amount</dt>
    <dd>
        ${{ $proposal->finalBusinessAmount() }}
        @if( Auth::user()->profile->type === 'provider' )
            <span class="text-danger pl1em">Without fee <b>${{ $proposal->finalProviderAmount() }}</b></span>
        @endif
    </dd>

    <dt>Estimate Time</dt>
    <dd>
        @if( $proposal->negotiation->accepted_by )
            {{ $proposal->negotiation->estimate_time }}
        @else
            {{ $proposal->estimate_time }}
        @endif
    </dd>
</dl>