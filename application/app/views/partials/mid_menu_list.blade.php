<?php
    if(!isset($items))
    {
        $items = [];
    }
?>
@section('mid-menu-list')

    <ul class="container">
        @foreach($items as $route => $label)
            <?php
                if( is_array($label) )
                {
                    $params = $label[2];
                    $route = $label[0];
                    $label = $label[1];
                }
                else
                {
                    $params = [];
                }
            ?>

            <li>
                {{-- HTML::linkRoute($route, $label, $params, ['class' => ($current == $route ? 'active' : '')]) --}}
                <a href="{{ URL::route($route, $params) }}" class="@if( $current == $route) active @endif">{{ $label }}</a>
            </li>
        @endforeach
    </ul>

@stop
