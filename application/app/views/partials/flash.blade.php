{{-- Used for displaing flash messages from controllers --}}

@if (Session::has('flash.message'))
    <div class="flash alert alert-{{ Session::get('flash.type') }}">
        <p>{{ Session::get('flash.message') }}</p>
    </div>
@endif
