<div class="tab-header-with-icon {{ $class or null }}">
    <p>{{ HTML::image('images/icons/' . $icon, $title) }} {{ $title }}</p>
</div>