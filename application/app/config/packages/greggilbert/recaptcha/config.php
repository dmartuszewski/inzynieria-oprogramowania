<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6Lf7Ye4SAAAAAEGtyYWuRAtynPdJJEdzX62ckgCF',
	'private_key'	=> '6Lf7Ye4SAAAAADrWsm-mNzo_b_jhALOJdEsLXUHm',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> '',
    'options' => array(
        'theme' => 'white',
    ),
	
);