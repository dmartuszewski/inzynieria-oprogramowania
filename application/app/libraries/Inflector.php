<?php

    class Inflector {

        /**
         * Converts camel case string to underscores
         * @param $camelCaseInput
         * @return string
         */
        public static function underscore($camelCaseInput) {
            preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $camelCaseInput, $matches);
            $ret = $matches[0];
            foreach ($ret as &$match) {
                $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
            }

            return implode('_', $ret);
        }
    }