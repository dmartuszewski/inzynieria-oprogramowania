<?php

class HTML extends \Illuminate\Support\Facades\HTML {


    public static function fileTypeIcon($type, $size = 48)
    {
        $allowedSizes = [16, 32, 48, 512];
        if( !in_array($size, $allowedSizes))
        {
            throw new Exception('File type icon size allowed only in one off the following: ' . join(', ', $allowedSizes));
        }

        $file = "images/mime_types/{$size}px/{$type}.png";

        if(is_readable(public_path($file)))
        {
            return self::image($file, $type);
        }
    }
}