<?php

class UsersGroupsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 DB::table('users_groups')->truncate();

		$usersGroups = [
            ['user_id' => 1, 'group_id' => 1],
            ['user_id' => 2, 'group_id' => 2],
            ['user_id' => 3, 'group_id' => 2],
            ['user_id' => 4, 'group_id' => 2],
        ];

		// Uncomment the below to run the seeder
		 DB::table('users_groups')->insert($usersGroups);
	}

}
