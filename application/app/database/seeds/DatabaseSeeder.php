<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		// $this->call('UserTableSeeder');
//		$this->call('GroupsTableSeeder');
//		$this->call('UsersTableSeeder');
//		$this->call('UsersGroupsTableSeeder');
		$this->call('ServiceTypesTableSeeder');
		$this->call('PayTypesTableSeeder');
		$this->call('BuildingsTableSeeder');
		$this->call('AlertsTableSeeder');
		$this->call('RoomsTableSeeder');
		$this->call('FeaturesTableSeeder');
	}

}