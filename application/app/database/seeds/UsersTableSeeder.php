<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 DB::table('users')->truncate();

		$users = [
            [
                'first_name' => 'Daniel',
                'last_name'  => 'Martuszewski',
                'email'      => 'dan@o2.pl',
                'password'   => Hash::make( 'password' ),
            ],
            [
                'first_name' => 'Agnieszka',
                'last_name'  => 'Pres',
                'email'      => 'apres@gmail.com',
                'password'   => Hash::make( 'password' ),
            ],
            [
                'first_name' => 'Patrycja',
                'last_name'  => 'Martuszewska',
                'email'      => 'pmart@gmail.com',
                'password'   => Hash::make( 'password' ),
            ],
            [
                'first_name' => 'Piotr',
                'last_name'  => 'Matras',
                'email'      => 'boss@matras.info.pl',
                'password'   => Hash::make( 'password' ),
            ],
		];

        for($i = 0, $n = count($users); $i < $n; ++$i) {
            $users[$i]['created_at'] = new DateTime;
        }

		// Uncomment the below to run the seeder
		 DB::table('users')->insert($users);
	}

}
