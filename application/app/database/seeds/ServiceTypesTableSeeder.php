<?php

class ServiceTypesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 DB::table('service_types')->truncate();

		$serviceTypes = [
            // Main service types
            ['id' => 1, 'parent_id' => null, 'name' => 'Online Media / Advertising'],
                ['id' => 2, 'parent_id' => 1, 'name' => 'General'],
                ['id' => 3, 'parent_id' => 1, 'name' => 'Banner Campaigns'],
                ['id' => 4, 'parent_id' => 1, 'name' => 'Email Campaigns'],
                ['id' => 5, 'parent_id' => 1, 'name' => 'Video Campaigns'],
                ['id' => 6, 'parent_id' => 1, 'name' => 'Social Media'],
                ['id' => 7, 'parent_id' => 1, 'name' => 'Search Engine Optimization'],
                ['id' => 8, 'parent_id' => 1, 'name' => 'Search Engine Marketing'],
                ['id' => 9, 'parent_id' => 1, 'name' => 'Direct Response'],
                ['id' => 19, 'parent_id' => 1, 'name' => 'Other'],

            ['id' => 20, 'parent_id' => null, 'name' => 'Public Relations'],
                ['id' => 21, 'parent_id' => 20, 'name' => 'General'],
                ['id' => 22, 'parent_id' => 20, 'name' => 'Online Releases'],
                ['id' => 23, 'parent_id' => 20, 'name' => 'Offline Releases'],
                ['id' => 39, 'parent_id' => 20, 'name' => 'Other'],

            ['id' => 40, 'parent_id' => null, 'name' => 'Business to Business'],
                ['id' => 41, 'parent_id' => 40, 'name' => 'General'],
                ['id' => 42, 'parent_id' => 40, 'name' => 'Online B2B'],
                ['id' => 43, 'parent_id' => 40, 'name' => 'Offline B2B'],
                ['id' => 44, 'parent_id' => 40, 'name' => 'Tradeshows'],
                ['id' => 59, 'parent_id' => 40, 'name' => 'Other'],

            ['id' => 60, 'parent_id' => null, 'name' => 'Direct Response'],
                ['id' => 61, 'parent_id' => 60, 'name' => 'General'],
                ['id' => 62, 'parent_id' => 60, 'name' => 'Online Direct Response'],
                ['id' => 63, 'parent_id' => 60, 'name' => 'Offline Direct Response'],
                ['id' => 64, 'parent_id' => 60, 'name' => 'Coupons'],
                ['id' => 65, 'parent_id' => 60, 'name' => 'Contests'],
                ['id' => 79, 'parent_id' => 60, 'name' => 'Other'],

            ['id' => 80, 'parent_id' => null, 'name' => 'Mobile Media / Advertising'],
                ['id' => 81, 'parent_id' => 80, 'name' => 'General'],
                ['id' => 82, 'parent_id' => 80, 'name' => 'Mobile Web'],
                ['id' => 83, 'parent_id' => 80, 'name' => 'In-App'],
                ['id' => 99, 'parent_id' => 80, 'name' => 'Other'],

            ['id' => 100, 'parent_id' => null, 'name' => 'Traditional Media / Advertising'],
                ['id' => 101, 'parent_id' => 100, 'name' => 'General'],
                ['id' => 102, 'parent_id' => 100, 'name' => 'Television Campaigns'],
                ['id' => 103, 'parent_id' => 100, 'name' => 'Radio Campaigns'],
                ['id' => 104, 'parent_id' => 100, 'name' => 'Billboard Campaigns'],
                ['id' => 105, 'parent_id' => 100, 'name' => 'Direct Mail'],
                ['id' => 106, 'parent_id' => 100, 'name' => 'Print Media'],
                ['id' => 107, 'parent_id' => 100, 'name' => 'Sponsorships'],
                ['id' => 119, 'parent_id' => 100, 'name' => 'Other'],

            ['id' => 120, 'parent_id' => null, 'name' => 'Graphic Design'],
                ['id' => 121, 'parent_id' => 120, 'name' => 'Online Ad Units'],
                ['id' => 122, 'parent_id' => 120, 'name' => 'Offline Ads'],
                ['id' => 123, 'parent_id' => 120, 'name' => 'Packaging'],
                ['id' => 124, 'parent_id' => 120, 'name' => 'Logo'],
                ['id' => 125, 'parent_id' => 120, 'name' => 'Brochure'],
                ['id' => 126, 'parent_id' => 120, 'name' => 'Newspaper'],
                ['id' => 127, 'parent_id' => 120, 'name' => 'Signage'],
                ['id' => 139, 'parent_id' => 120, 'name' => 'Other'],

            ['id' => 140, 'parent_id' => null, 'name' => 'Website Development'],
                ['id' => 141, 'parent_id' => 140, 'name' => 'General'],
                ['id' => 142, 'parent_id' => 140, 'name' => 'Design'],
                ['id' => 143, 'parent_id' => 140, 'name' => 'Programming'],
                ['id' => 144, 'parent_id' => 140, 'name' => 'Edits'],
                ['id' => 159, 'parent_id' => 140, 'name' => 'Other'],

            ['id' => 160, 'parent_id' => null, 'name' => 'Copywriting'],
                ['id' => 161, 'parent_id' => 160, 'name' => 'General'],
                ['id' => 162, 'parent_id' => 160, 'name' => 'Public Relations'],
                ['id' => 163, 'parent_id' => 160, 'name' => 'Online Content'],
                ['id' => 164, 'parent_id' => 160, 'name' => 'Social Media Content'],
                ['id' => 179, 'parent_id' => 160, 'name' => 'Other'],

            ['id' => 180, 'parent_id' => null, 'name' => 'Video Development'],
                ['id' => 181, 'parent_id' => 180, 'name' => 'General'],
                ['id' => 182, 'parent_id' => 180, 'name' => 'Online Preroll'],
                ['id' => 183, 'parent_id' => 180, 'name' => 'TV Spot'],
                ['id' => 184, 'parent_id' => 180, 'name' => 'Online Content'],
                ['id' => 185, 'parent_id' => 180, 'name' => 'Instructional'],
                ['id' => 186, 'parent_id' => 180, 'name' => 'Entertainment'],
                ['id' => 199, 'parent_id' => 180, 'name' => 'Other'],

            ['id' => 200, 'parent_id' => null, 'name' => 'Legal'],
                ['id' => 201, 'parent_id' => 200, 'name' => 'Advertising Copy'],
                ['id' => 202, 'parent_id' => 200, 'name' => 'Advertising General'],
                ['id' => 203, 'parent_id' => 200, 'name' => 'Contracting'],
                ['id' => 219, 'parent_id' => 200, 'name' => 'Other'],

            ['id' => 220, 'parent_id' => null, 'name' => 'Telemarketing'],
                ['id' => 221, 'parent_id' => 220, 'name' => 'General'],
                ['id' => 222, 'parent_id' => 220, 'name' => 'Call Center'],
                ['id' => 223, 'parent_id' => 220, 'name' => 'Interactive Voice Response'],
                ['id' => 239, 'parent_id' => 220, 'name' => 'Other'],

            ['id' => 240, 'parent_id' => null, 'name' => 'Market Research'],
                ['id' => 241, 'parent_id' => 240, 'name' => 'General'],
                ['id' => 242, 'parent_id' => 240, 'name' => 'Online Audience'],
                ['id' => 243, 'parent_id' => 240, 'name' => 'Offline'],
                ['id' => 244, 'parent_id' => 240, 'name' => 'Survey'],
                ['id' => 259, 'parent_id' => 240, 'name' => 'Other'],

            ['id' => 260, 'parent_id' => null, 'name' => 'General Consulting'],
                ['id' => 261, 'parent_id' => 260, 'name' => 'General'],
                ['id' => 262, 'parent_id' => 260, 'name' => 'General Marketing'],
                ['id' => 263, 'parent_id' => 260, 'name' => 'On-Site Consultant'],
                ['id' => 279, 'parent_id' => 260, 'name' => 'Other'],

            ['id' => 280, 'parent_id' => null, 'name' => 'Event Planning'],
                ['id' => 281, 'parent_id' => 280, 'name' => 'General'],
                ['id' => 282, 'parent_id' => 280, 'name' => 'Tradeshow'],
                ['id' => 283, 'parent_id' => 280, 'name' => 'Educational'],
                ['id' => 284, 'parent_id' => 280, 'name' => 'Sponsorship'],
                ['id' => 285, 'parent_id' => 280, 'name' => 'Product Launch'],
                ['id' => 286, 'parent_id' => 280, 'name' => 'Community'],
                ['id' => 299, 'parent_id' => 280, 'name' => 'Other'],
        ];

		// Uncomment the below to run the seeder
		DB::table('service_types')->insert($serviceTypes);
	}

}
