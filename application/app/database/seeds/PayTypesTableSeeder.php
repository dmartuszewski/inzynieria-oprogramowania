<?php

class PayTypesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 DB::table('pay_types')->truncate();

        $payTypes = [
            ['name' => 'Escrow'],
            ['name' => 'Up Front'],
            ['name' => 'Split'],
            ['name' => 'Hourly']
        ];

		// Uncomment the below to run the seeder
		 DB::table('pay_types')->insert($payTypes);
	}

}
