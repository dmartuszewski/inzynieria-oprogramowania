<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfileImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profile_images', function(Blueprint $table) {
			$table->increments('id');
            $table->unsignedInteger('upload_id');
            $table->unsignedInteger('uploadable_id');
            $table->string('uploadable_type');

            $table->foreign('upload_id')->references('id')->on('uploads')->onDelete('cascade');
            $table->foreign('uploadable_id')->references('id')->on('profiles')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profile_images');
	}

}
