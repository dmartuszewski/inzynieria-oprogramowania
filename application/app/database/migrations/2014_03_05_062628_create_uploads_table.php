<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('uploads', function(Blueprint $table) {
			$table->increments('id');
			$table->string('file', 50);
			$table->string('original_file_name', 50);
			$table->string('dir', 50);
			$table->integer('size');
			$table->string('type', 15);
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->string('of_type')->nullable();
			$table->unsignedInteger('of_id')->nullable();
			$table->datetime('deleted_at')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('uploads');
	}

}
