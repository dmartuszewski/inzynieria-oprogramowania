<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeatureRoomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feature_room', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('feature_id')->unsigned()->index();
			$table->unsignedInteger('room_id')->unsigned()->index();
			$table->integer('quantity')->unsigned();
			$table->text('notes');

			$table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');
			$table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feature_room');
	}

}
